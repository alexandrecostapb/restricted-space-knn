package base;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import util.JSATData;
import util.MyRatings;
import correlation.PearsonFunction;

public abstract class Recommender implements IRecommender {

	public int K;
	public MyRatings myRatings;
	public MyBaseLinePredictor myBaseline;
	public float Shrinkage;
	public String databaseName;
	public PearsonFunction pearson;
	public String recommenderName = "Recommender";
	public JSATData jsatData;
	public int count;
	
	public Recommender() {

	}

	public void Train() throws Exception {
		myBaseline = new MyBaseLinePredictor(myRatings);
		myBaseline.Train();
		pearson = new PearsonFunction();
		pearson.setData(myRatings, this.Shrinkage);
	}

	public void setData(MyRatings myRatings, String dataBaseName) {
		this.myRatings = myRatings;
		this.databaseName = dataBaseName;

		try {
			jsatData = new JSATData(myRatings, true);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public String getRecommenderName() {
		return recommenderName;
	}

	public void setRecommenderName(String recommenderName) {
		this.recommenderName = recommenderName;
	}

	public float getShrinkage() {
		return Shrinkage;
	}

	public void setShrinkage(float shrinkage) {
		Shrinkage = shrinkage;
	}

	public void setK(int k) {
		this.K = k;
	}

	public int getK() {
		return this.K;
	}

	public abstract double Predict(int user_id, int item_id) throws Exception;

	public abstract int getNeighborId(List list, int index) throws Exception;

	public double Prediction(int user_id, int item_id, List neighborList)
			throws Exception {
		count = 0;
		
		double result = this.myBaseline.Predict(user_id, item_id);

		double sum = 0;
		double weight_sum = 0;
		int neighbors = K;
		
		for (int i = 0; i < neighborList.size(); i++) {			
			int user_id2 = getNeighborId(neighborList, i);
//			System.out.println(i+"/"+neighborList.size()+" - USER: "+user_id + " ITEM: "+item_id
//					 +" Neighbor: "+user_id2);
			if (myRatings.hasRating(user_id2, item_id)) {
		
				double rating = myRatings.getRating(user_id2, item_id);
				double weight = pearson.computeCorrelation(user_id, user_id2);

				weight_sum += weight;
				sum += weight
						* (rating - this.myBaseline.Predict(user_id2, item_id));

				//count++;
				if (--neighbors == 0)
					break;
			}
		}

		if (weight_sum != 0)
			result += (sum / weight_sum);
		if (result > myRatings.MaxRating)
			result = myRatings.MaxRating;
		if (result < myRatings.MinRating)
			result = myRatings.MinRating;

		result = Math.round(result);
		//System.out.println(count);

		return result;
	}

	public double calculateMAE(MyRatings testData) throws Exception {

		double sum = 0;
		int count = 0;
		for (int i = 0; i < testData.AllUsers.size(); i++) {
			int userId = testData.AllUsers.get(i);
			HashMap<Integer, Float> itemsAndRatings = testData
					.getItemsAndRatings(userId);
			for (Map.Entry<Integer, Float> item : itemsAndRatings.entrySet()) {
				count++;
				double prediction = Predict(userId, item.getKey());
				double realRating = item.getValue();
				sum += Math.abs(prediction - realRating);
			}
		}

		return sum / count;
	}
}
