package base;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

import util.ListOperations;
import util.MyRatings;

public class MyBaseLinePredictor {

	// / <summary>Regularization parameter for the user biases</summary>
	public float RegU;

	// / <summary>Regularization parameter for the item biases</summary>
	public float RegI;

	// /
	public int NumIter;

	// / <summary>the global rating average</summary>
	protected float global_average;

	// / <summary>the user biases</summary>
	protected float[] user_biases;

	// / <summary>the item biases</summary>
	protected float[] item_biases;

	public MyRatings myRatings;
	public int MaxUserID;
	public int MaxItemID;

	
	// / <summary>Default constructor</summary>
	public MyBaseLinePredictor(MyRatings myRatings) {
		this.myRatings = myRatings;
		this.MaxUserID = ListOperations.maxInt(myRatings.AllUsers);
		this.MaxItemID =ListOperations.maxInt( myRatings.AllItems);
		

		RegU = 15;
		RegI = 10;
		NumIter = 10;

	}

	public void Train() throws IOException {
		user_biases = new float[MaxUserID + 1];
		item_biases = new float[MaxItemID + 1];

		global_average = myRatings.GlobalAverage;

		for (int i = 0; i < NumIter; i++)
			Iterate();

	}


	public void Iterate() throws IOException {
		OptimizeItemBiases();
		OptimizeUserBiases();
	}

	void OptimizeUserBiases() throws IOException
	        {
	            int[] user_ratings_count = new int[MaxUserID + 1];
	            for (int u = 0; u <= MaxUserID; u++)
	                user_biases[u] = 0;

	            for (int i = 0; i < myRatings.AllUsers.size(); i++)
	            {
	                int userId = myRatings.AllUsers.get(i);
	                HashMap<Integer, Float> itemsAndRatings = myRatings.getItemsAndRatings(userId);
	                for (Map.Entry<Integer, Float> item : itemsAndRatings.entrySet()) {
	                	
	                    user_biases[userId] += item.getValue() - global_average - item_biases[item.getKey()];
	                    user_ratings_count[userId]++;
	                }
	            }

	            for (int u = 0; u < user_biases.length; u++)
	                if (user_ratings_count[u] != 0)
	                    user_biases[u] = user_biases[u] / (RegU + user_ratings_count[u]);
	        }

	void OptimizeItemBiases()
	        {
	            int[] item_ratings_count = new int[MaxItemID + 1];
	            for (int i = 0; i <= MaxItemID; i++)
	                item_biases[i] = 0;
	            

	            for (int i = 0; i < myRatings.AllUsers.size(); i++)
	            {
	                int userId = myRatings.AllUsers.get(i);
	                HashMap<Integer, Float> itemsAndRatings = myRatings.getItemsAndRatings(userId);
	                for (Map.Entry<Integer, Float> item : itemsAndRatings.entrySet()) {	                	               	
	                    item_biases[item.getKey()] += item.getValue() - global_average - user_biases[userId];	                    
	                    item_ratings_count[item.getKey()]++;
	                    	            
	                }
	            }


	            for (int i = 0; i < item_biases.length; i++)
	                if (item_ratings_count[i] != 0)
	                    item_biases[i] = item_biases[i] / (RegI + item_ratings_count[i]);
	        }

	// /
	public float Predict(int user_id, int item_id) throws IOException {
		float user_bias = (user_id < user_biases.length && user_id >= 0) ? user_biases[user_id]
				: 0;
		float item_bias = (item_id < item_biases.length && item_id >= 0) ? item_biases[item_id]
				: 0;				
		float result = global_average + user_bias + item_bias;		

		if (result > myRatings.MaxRating)
			return myRatings.MaxRating;
		if (result < myRatings.MinRating)
			return myRatings.MinRating;

		return result;
	}
}
