package base;

import util.MyRatings;

public interface IRecommender {
	
	public void setK(int k);
	public void Train()throws Exception;
	public double calculateMAE(MyRatings myRatings) throws Exception;
	public void setData(MyRatings myRatings,String trainDataName) throws Exception;
	public void setShrinkage(float shrinkage);
	public String getRecommenderName();

}
