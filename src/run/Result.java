package run;

public class Result {

	public double MAE;
	public double trainTime;
	public double evaluationTime;
	public int folderNumber;
	public String recommenderName;
	public int dataBaseCount;
	public String dataBaseName;

	public double getMAE() {
		return MAE;
	}

	public void setMAE(double mAE) {
		MAE = mAE;
	}

	public double getTrainTime() {
		return trainTime;
	}

	public void setTrainTime(double trainTime) {
		this.trainTime = trainTime;
	}

	public double getEvaluationTime() {
		return evaluationTime;
	}

	public void setEvaluationTime(double evaluationTime) {
		this.evaluationTime = evaluationTime;
	}

	public int getFolderNumber() {
		return folderNumber;
	}

	public void setFolderNumber(int folderNumber) {
		this.folderNumber = folderNumber;
	}

	public String getRecommenderName() {
		return recommenderName;
	}

	public void setRecommenderName(String recommenderName) {
		this.recommenderName = recommenderName;
	}

	public int getDataBaseCount() {
		return dataBaseCount;
	}

	public void setDataBaseCount(int dataBaseCount) {
		this.dataBaseCount = dataBaseCount;
	}

	public String getDataBaseName() {
		return dataBaseName;
	}

	public void setDataBaseName(String dataBaseName) {
		this.dataBaseName = dataBaseName;
	}


	public String toString() {
		StringBuilder sb = new StringBuilder();

		sb.append("Folder: " + folderNumber + " - " + "RECOMMENDER: "
				+ recommenderName);
		sb.append("\n");
		sb.append("Folder: " + folderNumber + " - " + "MAE: " + MAE);
		sb.append("\n");
		sb.append("Folder: " + folderNumber + " - " + "TRAIN TIME: "
				+ trainTime + " seconds");
		sb.append("\n");
		sb.append("Folder: " + folderNumber + " - " + "EVALUATION TIME: "
				+ evaluationTime + " seconds");


		return sb.toString();

	}

}
