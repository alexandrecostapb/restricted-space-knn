package run;

import gn.GNRecommender;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import jsat.KDtreeRecommender;
import jsat.LSHRecommender;
import jsat.VPtreeRecommender;
import classic.ClassicKNN;
import my.MyRecommender;
import util.DataSaver;
import util.MyRatings;
import base.IRecommender;

public class Execution {

	public final String ROOT = "datasets/";

	public StringBuilder sb;
	public StringBuilder excelFile;
	public StringBuilder heuristicFile;

	public static final int CLASSIC = 1;
	public static final int MY = 2;
	public static final int KD_TREE = 3;
	public static final int VP_TREE = 4;
	public static final int LSH = 5;
	public static final int GN = 6;

	public int gnSize;

	public int count;

	public String recommenderName;

	public Execution() {

	}

	public IRecommender chooseRecommender(int key) {
		switch (key) {
		case CLASSIC:
			return new ClassicKNN();

		case MY:
			int globalNeighborsPercentage = 15;
			return new MyRecommender(globalNeighborsPercentage);

		case KD_TREE:
			return new KDtreeRecommender();

		case VP_TREE:
			return new VPtreeRecommender();

		case LSH:
			return new LSHRecommender();

		case GN:
			return new GNRecommender();

		default:
			System.out.println("Invalid choice of recommender!");
			return null;
		}
	}

	public void addResult(double trainTime, double evaluationTime, double MAE,
			int folderNumber, String recommenderName) {
		sb.append("Folder: " + folderNumber + " - " + "RECOMMENDER: "
				+ recommenderName);
		sb.append("\n");
		sb.append("Folder: " + folderNumber + " - " + "MAE: " + MAE);
		sb.append("\n");
		sb.append("Folder: " + folderNumber + " - " + "TRAIN TIME: "
				+ trainTime + " seconds");
		sb.append("\n");
		sb.append("Folder: " + folderNumber + " - " + "EVALUATION TIME: "
				+ evaluationTime + " seconds");
		sb.append("\n");
		sb.append("\n");
	}

	public void run(int recommenderChoice) throws Exception {

		gnSize = 0;
		sb = new StringBuilder();
		excelFile = new StringBuilder();
		heuristicFile = new StringBuilder();
		count = 1;
		int dataBaseCount = 10;
		int folderCount = 1;
		int k = 30;
		float shrinkage = 25;
		String folderName = "100k";

		double maeFolder = 0.0;
		double trainTimeFolder = 0.0;
		double evaluationTimeFolder = 0.0;
		double heuristicFolder = 0.0;

		double maeFolderSum = 0.0;
		double trainTimeFolderSum = 0.0;
		double evaluationTimeFolderSum = 0.0;
		double heuristicFolderSum = 0.0;

		sb.append("DATABASE NAME: " + folderName);
		sb.append("\n");
		sb.append("FOLDERS: " + folderCount);
		sb.append("\n");
		sb.append("DATABASES PER FOLDER: " + dataBaseCount
				+ " of trainning and " + dataBaseCount + " of testing.");
		sb.append("\n");
		sb.append("K: " + k);
		sb.append("\n");
		sb.append("\n");

		excelFile.append(sb.toString());
		excelFile.append("NUMBER" + "	" + "TRAIN" + "	" + "EVALUATION" + "	"
				+ "MAE");
		excelFile.append("\n");

		heuristicFile.append(sb.toString());
		heuristicFile.append("NUMBER" + "	" + "TIME" + "	" + "MAE");
		heuristicFile.append("\n");

		for (int i = 1; i <= folderCount; i++) {

			Result result = runRecommender(recommenderChoice, folderName, i,
					dataBaseCount, k, shrinkage);
			result.setFolderNumber(i);
			result.setDataBaseCount(dataBaseCount);
			result.setDataBaseName(folderName);

			// System.out.println(result.toString());
			// System.out.println();

			trainTimeFolder = result.getTrainTime();
			evaluationTimeFolder = result.getEvaluationTime();
			maeFolder = result.getMAE();

			// addResult(trainTimeFolder, evaluationTimeFolder, maeFolder, i,
			// result.getRecommenderName());

			trainTimeFolderSum += trainTimeFolder;
			evaluationTimeFolderSum += evaluationTimeFolder;
			maeFolderSum += maeFolder;
			heuristicFolderSum += heuristicFolder;
		}

		double finalTrainTime = trainTimeFolderSum / folderCount;
		double finalEvaluationTime = evaluationTimeFolderSum / folderCount;
		double finalMAE = maeFolderSum / folderCount;
		double finalHeuristic = heuristicFolderSum / folderCount;

		sb.append("======================================================");
		sb.append("\n");
		addResult(finalTrainTime, finalEvaluationTime, finalMAE, 0,
				recommenderName);

		String time = new SimpleDateFormat("dd-MM-yyyy_HH.mm.ss")
				.format(Calendar.getInstance().getTime());

		if (recommenderName.equals("GN")) {
			gnSize = gnSize / count;
			recommenderName += "-" + gnSize;
		}
		

		DataSaver.save(sb.toString(), ROOT + "RESULTS_" + recommenderName + "_"
				+ folderName + "_" + time + ".txt");
		DataSaver.save(excelFile.toString(), ROOT + "RESULTS_"
				+ recommenderName + "_" + folderName + "_" + time + ".excel");
		DataSaver.save(heuristicFile.toString(), ROOT + "RESULTS_"
				+ recommenderName + "_" + folderName + "_" + time
				+ ".heuristic");

	}

	private Result runRecommender(int recommenderChoice, String folderName,
			int folderNumber, int dataBaseCount, int k, float shrinkage)
			throws Exception {

		IRecommender recommender = null;

		double currentMAE = 0.0;
		double currentTrainTime = 0.0;
		double currentEvaluationTime = 0.0;
		double currentHeuristicTime = 0.0;

		double maeSum = 0.0;
		double evaluationTimeSum = 0.0;
		double trainTimeSum = 0.0;
		double heuristicTimeSum = 0.0;

		for (int i = 1; i <= dataBaseCount; i++) {

			recommender = chooseRecommender(recommenderChoice);
			recommenderName = recommender.getRecommenderName();

			if (recommenderName.equals("GN")) {
				((GNRecommender) recommender).folderName = folderName;
				((GNRecommender) recommender).folderNumber = folderNumber;
				gnSize += ((GNRecommender) recommender).gnSize;
			}

			String trainDataName = "10-fold-" + i + "-train-" + folderName;
			String testDataName = "10-fold-" + i + "-test-" + folderName;

			String trainDataPath = ROOT + folderName + "/" + folderNumber + "/"
					+ trainDataName + ".dat";
			String testDataPath = ROOT + folderName + "/" + folderNumber + "/"
					+ testDataName + ".dat";

			MyRatings myRatings_train_data = new MyRatings();
			myRatings_train_data.createData(trainDataPath);

			MyRatings myRatings_test_data = new MyRatings();
			myRatings_test_data.createData(testDataPath);

			recommender.setData(myRatings_train_data, trainDataName);

			recommender.setK(k);

			recommender.setShrinkage(shrinkage);

			long trainStartTime = System.currentTimeMillis();
			recommender.Train();
			long trainEndTime = System.currentTimeMillis();
			currentTrainTime = (trainEndTime - trainStartTime) / 1000.0;

			long evaluationStartTime = System.currentTimeMillis();
			currentMAE = recommender.calculateMAE(myRatings_test_data);
			long evaluationEndTime = System.currentTimeMillis();
			currentEvaluationTime = (evaluationEndTime - evaluationStartTime) / 1000.0;

			trainTimeSum += currentTrainTime;
			evaluationTimeSum += currentEvaluationTime;
			maeSum += currentMAE;

			if (recommenderName.equals("GN")) {
				gnSize += ((GNRecommender) recommender).gnSize;
			}

			if (recommenderName.equals("MY")) {
				currentHeuristicTime = ((MyRecommender) recommender).heuristicTime;
				heuristicTimeSum += currentHeuristicTime;

			}
			Result currentResult = new Result();
			currentResult.setTrainTime(currentTrainTime);
			currentResult.setEvaluationTime(currentEvaluationTime);
			currentResult.setMAE(currentMAE);
			currentResult.setRecommenderName(recommender.getRecommenderName());
			currentResult.setFolderNumber(count);
			currentResult.setDataBaseCount(dataBaseCount);
			currentResult.setDataBaseName(folderName);

			System.out.println(currentResult.toString());
			System.out.println();

			addResult(currentTrainTime, currentEvaluationTime, currentMAE,
					count, folderName);
			addToExcelFile(currentTrainTime, currentEvaluationTime, currentMAE,
					count);
			addToHeuristicFile(currentHeuristicTime, currentMAE, count);

			count++;
			System.out.println(count);

		}

		Result result = new Result();
		result.setTrainTime(trainTimeSum / dataBaseCount);
		result.setEvaluationTime(evaluationTimeSum / dataBaseCount);
		result.setMAE(maeSum / dataBaseCount);
		result.setRecommenderName(recommender.getRecommenderName());

		

		return result;

	}

	public void addToExcelFile(double trainTime, double evaluationTime,
			double MAE, int folderNumber) {
		excelFile.append(folderNumber + "	" + trainTime + "	" + evaluationTime
				+ "	" + MAE);
		excelFile.append("\n");
	}

	public void addToHeuristicFile(double time, double MAE, int folderNumber) {
		heuristicFile.append(folderNumber + "	" + time + "	" + MAE);
		heuristicFile.append("\n");
	}
}
