package jsat;

import java.util.List;

import jsat.linear.Vec;
import jsat.linear.distancemetrics.DistanceMetric;
import jsat.linear.distancemetrics.EuclideanDistance;
import jsat.linear.vectorcollection.lsh.E2LSH;
import util.JSATData;
import base.Recommender;

public abstract class JSATRecommender extends Recommender {

	public DistanceMetric distance;
	public List<Vec> dataset;

	public JSATRecommender() {
		setRecommenderName("JSAT");
	}

	@Override
	public void Train() throws Exception {
		super.Train();
		distance = new EuclideanDistance();
		dataset = jsatData.VectorList;
	}

	public int getNeighborId(List neighborsList, int index) throws Exception {
		return jsatData.getUserId((Vec) neighborsList.get(index));
	}

	public abstract double Predict(int user_id, int item_id) throws Exception;

}
