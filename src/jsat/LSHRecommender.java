package jsat;

import java.util.List;

import jsat.linear.Vec;
import jsat.linear.vectorcollection.lsh.E2LSH;

public class LSHRecommender extends JSATRecommender {

	public E2LSH<Vec> lsh;

	public LSHRecommender() {
		setRecommenderName("LSH");
	}

	@Override
	public void Train() throws Exception {
		super.Train();
		int radius = getK();
		double errorApproximation = 0.01;
		int projectionRadius = 4;
		int hashFunctions = 5;
		double correctNeighborsProb =  0.3;
		
		lsh = new E2LSH<Vec>(dataset, radius, errorApproximation, projectionRadius, hashFunctions, correctNeighborsProb, distance);
	}

	public int getNeighborId(List neighborsList, int index) throws Exception {
		return jsatData.getUserId((Vec) neighborsList.get(index));
	}

	public double Predict(int user_id, int item_id) throws Exception {
		Vec vectorQuery = jsatData.getVector(user_id);
		List neighborList = lsh.searchR(vectorQuery,true);
		
		return super.Prediction(user_id, item_id, neighborList);

	}

}
