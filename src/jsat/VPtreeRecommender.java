package jsat;

import java.util.List;

import jsat.linear.Vec;
import jsat.linear.vectorcollection.VPTree;
import util.JSATData;

public class VPtreeRecommender extends JSATRecommender {

	public VPTree vpTree;

	public VPtreeRecommender() {
		setRecommenderName("VP-TREE");
	}

	@Override
	public void Train() throws Exception {
		super.Train();
		vpTree = new VPTree(dataset, distance);
	}

	public double Predict(int user_id, int item_id) throws Exception {
		Vec vectorQuery = jsatData.getVector(user_id);
		List neighborList = vpTree.search(vectorQuery, K);
		
		return super.Prediction(user_id, item_id, neighborList);

	}

}
