package jsat;

import java.util.List;

import jsat.linear.Vec;
import jsat.linear.vectorcollection.KDTree;
import util.JSATData;

public class KDtreeRecommender extends JSATRecommender {

	public KDTree kdTree;

	public KDtreeRecommender() {
		setRecommenderName("KD-TREE");
	}

	@Override
	public void Train() throws Exception {
		super.Train();	
		kdTree = new KDTree(dataset, distance);
	}

	public double Predict(int user_id, int item_id) throws Exception {
		Vec vectorQuery = jsatData.getVector(user_id);
		List neighborList = kdTree.search(vectorQuery, K);
	
		return super.Prediction(user_id, item_id, neighborList);

	}

}
