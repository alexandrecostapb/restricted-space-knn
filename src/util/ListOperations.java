package util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class ListOperations {

	public static int sumInt(List<Integer> list) {
		int sum = 0;
		for (int i = 0; i < list.size(); i++) {
			sum += list.get(i);
		}
		return sum;
	}

	public static float sumFloat(List<Float> list) {
		float sum = 0;
		for (int i = 0; i < list.size(); i++) {
			sum += list.get(i);
		}
		return sum;
	}

	public static float average(List list) {
		float average = 0;
		for (int i = 0; i < list.size(); i++) {
			average += (float) list.get(i);
		}
		return average / list.size();
	}

	public static int maxInt(List<Integer> list) {
		int max = list.get(0);
		for (int i = 0; i < list.size(); i++) {
			if (list.get(i) > max) {
				max = list.get(i);
			}
		}
		return max;
	}

	public static float maxFloat(List<Float> list) {
		float max = list.get(0);
		for (int i = 0; i < list.size(); i++) {
			if (list.get(i) > max) {
				max = list.get(i);
			}
		}
		return max;
	}


	public static String arrayToString(int[] array) {

		String s = "[";
		for (int i = 0; i < array.length; i++) {
			s += array[i] + ",";
		}
		s += "]";
		return s;

	}

	public static String arrayToString(float[] array) {

		String s = "[";
		for (int i = 0; i < array.length; i++) {
			String element = array[i] + "";
			s += element.substring(0, 3) + ",";
		}
		s += "]";
		return s;

	}

	public static <K extends Comparable,V extends Comparable> Map<K,V> sortByValuesDescending(Map<K,V> map){
        List<Map.Entry<K,V>> entries = new LinkedList<Map.Entry<K,V>>(map.entrySet());
      
        Collections.sort(entries, new Comparator<Map.Entry<K,V>>() {

            @Override
            public int compare(Entry<K, V> o1, Entry<K, V> o2) {
                return o2.getValue().compareTo(o1.getValue());
            }
        });
      
        //LinkedHashMap will keep the keys in the order they are inserted
        //which is currently sorted on natural ordering
        Map<K,V> sortedMap = new LinkedHashMap<K,V>();
      
        for(Map.Entry<K,V> entry: entries){
            sortedMap.put(entry.getKey(), entry.getValue());
        }
      
        return sortedMap;
    }
	
	public static <K extends Comparable,V extends Comparable> Map<K,V> sortByValuesAscending(Map<K,V> map){
        List<Map.Entry<K,V>> entries = new LinkedList<Map.Entry<K,V>>(map.entrySet());
      
        Collections.sort(entries, new Comparator<Map.Entry<K,V>>() {

            @Override
            public int compare(Entry<K, V> o1, Entry<K, V> o2) {
                return o1.getValue().compareTo(o2.getValue());
            }
        });
      
        //LinkedHashMap will keep the keys in the order they are inserted
        //which is currently sorted on natural ordering
        Map<K,V> sortedMap = new LinkedHashMap<K,V>();
      
        for(Map.Entry<K,V> entry: entries){
            sortedMap.put(entry.getKey(), entry.getValue());
        }
      
        return sortedMap;
    }
	
	public static <T> List<T> subtract(List<T> list1, List<T> list2) {  
        List<T> result = new ArrayList<T>();  
        Set<T> set2 = new HashSet<T>(list2);  
        for (T t1 : list1) {  
            if( !set2.contains(t1) ) {  
                result.add(t1);  
            }  
        }  
        return result;  
    }  

}
