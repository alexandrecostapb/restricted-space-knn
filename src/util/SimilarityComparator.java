package util;

import java.util.Comparator;

public class SimilarityComparator implements Comparator<Test> {

	@Override
	public int compare(Test obj1, Test obj2) {
		if (obj1.getSimmilarity() > obj2.getSimmilarity()) {
			return -1;
		} else if (obj1.getSimmilarity() < obj2.getSimmilarity()) {
			return 1;
		}
		return 0;
	}

}
