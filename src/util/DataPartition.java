package util;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

public class DataPartition {

	public static  final String MAIN_PATH = "results/";

	public static void createCrossValidationDataFiles(String dataBaseName,
			String originalFilePath, int nFold) throws IOException {
		nFoldTestCrossValidation(dataBaseName, originalFilePath, nFold);
		for (int i = 0; i < nFold; i++) {
			System.out.println("Creating Train data " + (i + 1));
			nFoldTrainCrossValidation2(dataBaseName, MAIN_PATH + nFold
					+ "-fold-" + (i + 1) + "-test-" + dataBaseName + ".dat",
					nFold, originalFilePath, i + 1);
		}

	}

	private static void nFoldTestCrossValidation(String dataBaseName,
			String filePath, int nFold) throws IOException {

		ArrayList<StringBuilder> dataFile = new ArrayList<StringBuilder>();
		ArrayList<String> lineArrayArrayList = new ArrayList<String>();
		BufferedReader file = new BufferedReader(new FileReader(filePath));
		String line = file.readLine();
		while (line != null) {
			if (!line.equals("")) {
				lineArrayArrayList.add(line);
			}
			line = file.readLine();
		}
		file.close();
		Collections.shuffle(lineArrayArrayList);

		for (int i = 0; i < nFold; i++) {
			dataFile.add(new StringBuilder());
		}

		int totalOfLines = 0;
		int index = 0;
		while (totalOfLines < lineArrayArrayList.size()) {
			dataFile.get(index).append(lineArrayArrayList.get(totalOfLines));
			dataFile.get(index).append("\n");
			index++;
			totalOfLines++;
			if (index >= nFold) {
				index = 0;
			}
		}

		for (int i = 0; i < nFold; i++) {
			DataSaver.save(dataFile.get(i).toString(), MAIN_PATH + nFold
					+ "-fold-" + (i + 1) + "-test-" + dataBaseName + ".dat");
		}
	}
	
	  private static void nFoldTrainCrossValidation2(String dataBaseName, String testFilePath, int nFold, String originalFilePath, int index) throws IOException
      {
          ArrayList<StringBuilder> trainDataFile = new ArrayList<StringBuilder>();
          ArrayList<String> testFileLinesArrayList = new ArrayList<String>();
    
          BufferedReader file = new BufferedReader(new FileReader(testFilePath));
          String line = file.readLine();
          while (line != null)
          {
              if (!line.equals(""))
              {
                  testFileLinesArrayList.add(line);
              }
              line = file.readLine();
          }
          file.close();

          ArrayList<String> originalFileLinesArrayList = new ArrayList<String>();
          file = new  BufferedReader(new FileReader(originalFilePath));
          line = file.readLine();
          while (line != null)
          {
              if (!line.equals(""))
              {
                  originalFileLinesArrayList.add(line);
              }
              line = file.readLine();
          }
          file.close();
          
          originalFileLinesArrayList = (ArrayList<String>) ListOperations.subtract(originalFileLinesArrayList, testFileLinesArrayList);
          

          StringBuilder trainData = new StringBuilder();
          for (int i = 0; i < originalFileLinesArrayList.size(); i++)
          {
              trainData.append(originalFileLinesArrayList.get(i));
              trainData.append("\n");

          }
          DataSaver.save(trainData.toString(), MAIN_PATH + nFold + "-fold-" + (index) + "-train-" + dataBaseName + ".dat");
      }
}
