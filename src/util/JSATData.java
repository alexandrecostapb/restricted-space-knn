package util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jsat.linear.DenseVector;
import jsat.linear.SparseVector;
import jsat.linear.Vec;

public class JSATData {
	 public MyRatings myRatings ;
     public HashMap<Integer, Vec> userIdVectorMap ;
     public HashMap<Vec, Integer> VectorUserIdMap ;
     public List<Vec> VectorList ;

     public JSATData(MyRatings myRatings, boolean isSparseVector) throws Exception
     {
         this.myRatings = myRatings;
         userIdVectorMap = new HashMap<Integer, Vec>();
         VectorUserIdMap = new HashMap<Vec, Integer>();
         VectorList = new ArrayList<Vec>();
         if (isSparseVector)
         {
             generateSparseVectorList();
         }
         else
         {
             generateDenseVectorList();
         }
         
//         for (Map.Entry<Vec, Integer> entry : VectorUserIdMap.entrySet()) {
//        	 Vec key = entry.getKey();
//        	 Integer value = entry.getValue();
//        	 System.out.println("VEC ID: "+key.toString());
//        	 System.out.println("Value: "+value);
//        	 System.out.println();
//    
//        	}
         
     }

     public Vec getVector(int userId) throws Exception
     { 
         if (userIdVectorMap.containsKey(userId))
         {
             return userIdVectorMap.get(userId);
         }
         System.out.println("nao temmmmmmmmmmmmmmmmmmmmm");
         throw new Exception("User ID does not exist");
     }

     public int getUserId(Vec vector) throws Exception
     {
         if (VectorUserIdMap.containsKey(vector))
         {
             return VectorUserIdMap.get(vector);
         }
         throw new Exception("Vector does not exist");
     }


     private void generateDenseVectorList() throws Exception
     {
         int dimensionality = myRatings.AllItems.size();
         for (int i = 0; i < myRatings.AllUsers.size(); i++)
         {
             int userId = myRatings.AllUsers.get(i);
             double ratingAverage = myRatings.getRatingAverage(userId);
             double[] userData = new double[dimensionality];
             for (int j = 0; j < myRatings.AllItems.size(); j++)
             {
                 int itemId = myRatings.AllItems.get(j);
                 if (myRatings.hasRating(userId, itemId))
                 {
                     userData[j] = myRatings.getRating(userId, itemId);
                     //userData[j] = 1;
                 }
                 else
                 {
                     userData[j] = ratingAverage;
                     //userData[j] = 0;
                 }

             }
             DenseVector denseVector = new DenseVector(userData);
             VectorList.add(denseVector);
             userIdVectorMap.put(userId, denseVector);
             VectorUserIdMap.put(denseVector, userId);

         }

     }


     public void generateSparseVectorList() throws Exception
     {
         int dimensionality = myRatings.AllItems.size();
         for (int i = 0; i < myRatings.AllUsers.size(); i++)
         {
             int userId = myRatings.AllUsers.get(i);
             double ratingAverage = myRatings.getRatingAverage(userId);
             SparseVector sparseVector = new SparseVector(dimensionality, myRatings.getRatingCount(userId));
             for (int j = 0; j < myRatings.AllItems.size(); j++)
             {
                 int itemId = myRatings.AllItems.get(j);
                 if (myRatings.hasRating(userId, itemId))
                 {
                     float rating = myRatings.getRating(userId, itemId);
                     sparseVector.increment(j, rating);
                 }


             }
             VectorList.add(sparseVector);
             userIdVectorMap.put(userId, sparseVector);
             VectorUserIdMap.put(sparseVector, userId);

         }
     }

}
