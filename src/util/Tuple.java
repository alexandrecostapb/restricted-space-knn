package util;

public class Tuple {

	public int userId;
	public int itemId;
	public float rating;

	public Tuple(int userId, int itemId, float rating) {
		this.userId = userId;
		this.itemId = itemId;
		this.rating = rating;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getItemId() {
		return itemId;
	}

	public void setItemId(int itemId) {
		this.itemId = itemId;
	}

	public float getRating() {
		return rating;
	}

	public void setRating(float rating) {
		this.rating = rating;
	}

}
