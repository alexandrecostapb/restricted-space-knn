package util;

import java.io.BufferedReader;
import java.io.Console;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MyRatings {

	public HashMap<Integer, HashMap<Integer, Float>> userItemData;
	public HashMap<Integer, Float> RatingAverage;
	public HashMap<Integer, Integer> NumberOfItemsRated;
	public HashMap<Integer, Integer> indexUserIdMap;
	public HashMap<Integer, Integer> indexItemIdMap;

	public List<Integer> AllUsers;
	public List<Integer> AllItems;
	public float MaxRating;
	public float MinRating;
	public float GlobalAverage;



	public HashMap<Integer, float[]> userVector;

	public MyRatings() {
		indexUserIdMap = new HashMap<Integer, Integer>();
		indexItemIdMap = new HashMap<Integer, Integer>();
		userItemData = new HashMap<Integer, HashMap<Integer, Float>>();
		AllUsers = new ArrayList<Integer>();
		AllItems = new ArrayList<Integer>();
		RatingAverage = new HashMap<Integer, Float>();
		NumberOfItemsRated = new HashMap<Integer, Integer>();
		GlobalAverage = 0;
		MaxRating = 0;
		MinRating = 0;

		this.userVector = new HashMap<Integer, float[]>();

	}

	private void createIndexUserIdMap(List<Integer> userIdList) {
		for (int i = 0; i < userIdList.size(); i++) {
			indexUserIdMap.put(userIdList.get(i), i);
		}
	}

	private void createIndexItemIdMap(List<Integer> itemIdList) {
		for (int i = 0; i < itemIdList.size(); i++) {
			indexItemIdMap.put(itemIdList.get(i), i);
		}
	}

	public int getUserIndex(int userId) throws Exception {
		if (indexUserIdMap.containsKey(userId)) {
			return indexUserIdMap.get(userId);
		}
		throw new Exception("User ID does note exist!");

	}

	public int getItemIndex(int itemId) throws Exception {
		if (indexItemIdMap.containsKey(itemId)) {
			return indexItemIdMap.get(itemId);
		}
		throw new Exception("Item ID does note exist!");

	}

	public void createData(String filePath) throws IOException {
		BufferedReader file = new BufferedReader(new FileReader(filePath));
		String line = file.readLine();

		float rating = Float.parseFloat(line.split(" ")[2]);
		MaxRating = rating;
		MinRating = rating;
		int count = 0;

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		// System.out.println("Inicial Rating: " + rating);

		while (line != null) {
			if (!line.equals("")) {
				String[] lineArray = line.split(" ");
				int userId = Integer.parseInt(lineArray[0]);
				int itemId = Integer.parseInt(lineArray[1]);
				rating = Float.parseFloat(lineArray[2]);

				count++;
				GlobalAverage += rating;

				if (MaxRating < rating) {
					MaxRating = rating;
				}

				if (MinRating > rating) {
					MinRating = rating;
				}

				if (!AllItems.contains(itemId)) {
					AllItems.add(itemId);
				}

				if (!userItemData.containsKey(userId)) {
					userItemData.put(userId,
							new HashMap<Integer, Float>());
				}
				if (userItemData.get(userId).containsKey(itemId)) {
					System.out.println("Item ja exist");
					br.readLine();
				} else {
					userItemData.get(userId).put(itemId, rating);
				}

			}
			line = file.readLine();
		}
		file.close();

		GlobalAverage = (float) (GlobalAverage / count);
		AllUsers = new ArrayList<Integer>(userItemData.keySet());
		Collections.sort(AllUsers);
		Collections.sort(AllItems);
		createUserData();
		createIndexUserIdMap(this.AllUsers);
		createIndexItemIdMap(this.AllItems);

	}

	public int getRatingCount(int userId) {
		return userItemData.get(userId).size();
	}

	public float getRatingAverage(int userId) {
		float average = 0;

		float sum = ListOperations.sumFloat(new ArrayList<>(userItemData.get(
				userId).values()));
		int quantity = userItemData.get(userId).size();

		average = (float) (sum / quantity);

		return average;
	}

	public float getRating(int userId, int itemId) throws Exception{

		if (userItemData.containsKey(userId)) {
			if (userItemData.get(userId).containsKey(itemId)) {
				return userItemData.get(userId).get(itemId);
			} else {
				throw new Exception("Item does not exist!");
			}
		} else {
			throw new Exception("User does not exist");
		}

	}

	public float[] getUserItemVector(int userId) throws Exception  {

		if (userVector.containsKey(userId)) {
			return userVector.get(userId);
		}

		float[] userItemVector = new float[this.AllItems.size()];
		for (int i = 0; i < this.AllItems.size(); i++) {
			int itemId = this.AllItems.get(i);
			if (hasRating(userId, itemId)) {
				userItemVector[i] = getRating(userId, itemId);
			} else {
				userItemVector[i] = getRatingAverage(userId);
			}
		}
		userVector.put(userId, userItemVector);

		return userItemVector;
	}

	public boolean hasRating(int userId, int itemId) throws Exception  {
		if (userItemData.containsKey(userId)) {
			if (userItemData.get(userId).containsKey(itemId)) {
				return true;
			} else {
				return false;
			}
		} else {
			throw new Exception("User does not exist");
		}


	}

	public List<Integer> getItems(int userId) {
		return new ArrayList<Integer>(userItemData.get(userId).keySet());
	}

	public HashMap<Integer, Float> getItemsAndRatings(int userId) {
		return userItemData.get(userId);
	}


	public void createUserData() {

		for (int i = 0; i < AllUsers.size(); i++) {
			int userId = AllUsers.get(i);

			float average = ListOperations.average(new ArrayList<>(userItemData
					.get(userId).values()));
			RatingAverage.put(userId, average);
			NumberOfItemsRated.put(userId, userItemData.get(userId).size());
		}

	}
}
