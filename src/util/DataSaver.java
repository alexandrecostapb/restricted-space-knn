package util;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class DataSaver {
	
	 public static void save(String text, String filepath) throws IOException
     {
         // create a writer and open the file
		 BufferedWriter writer = new BufferedWriter(new FileWriter(filepath));

         // write a line of text to the file
		 writer.write(text);

         // close the stream
		 writer.close();
     }

}
