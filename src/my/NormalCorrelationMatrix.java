package my;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import util.ListOperations;
import util.MyRatings;
import correlation.ICorrelationMatrix;
import correlation.PearsonFunction;

public class NormalCorrelationMatrix implements ICorrelationMatrix{
	public float[][] CorrelationMatrix;
	public ArrayList<Integer> GlobalNeighbors;
	public ArrayList<Integer> OtherUsers;
	public int rowLength;
	public int columnLength;
	public PearsonFunction pearsonCorrelation;
	public HashMap<Integer, Integer> rowIndex;
	public HashMap<Integer, Integer> columnIndex;
	public MyRatings myRatings;

	public NormalCorrelationMatrix(PearsonFunction correlationFunction,
			ArrayList<Integer> globalNeighbors, ArrayList<Integer> otherUsers) {
		this.OtherUsers = otherUsers;
		this.GlobalNeighbors = globalNeighbors;
		this.rowLength = otherUsers.size();
		this.columnLength = GlobalNeighbors.size();
		this.pearsonCorrelation = correlationFunction;
		createIndexMapping();
	}

	public void createIndexMapping() {
		rowIndex = new HashMap<Integer, Integer>();
		columnIndex = new HashMap<Integer, Integer>();
		for (int i = 0; i < this.rowLength; i++) {
			rowIndex.put(this.OtherUsers.get(i), i);
		}
		for (int j = 0; j < this.columnLength; j++) {
			columnIndex.put(this.GlobalNeighbors.get(j), j);
		}
	}

	public void fillCorrelationMatrix() {
		this.CorrelationMatrix = new float[this.rowLength][this.columnLength];
		for (int i = 0; i < this.rowLength; i++) {
			for (int j = 0; j < this.columnLength; j++) {
				CorrelationMatrix[i][j] = pearsonCorrelation
						.computeCorrelation(OtherUsers.get(i),
								GlobalNeighbors.get(j));
			}
		}
	}

	public float getCorrelation(int userId1, int userId2) {
		return CorrelationMatrix[rowIndex.get(userId1)][columnIndex
				.get(userId2)];
	}

	public ArrayList<Integer> GetPositivelyCorrelatedNeighbors(int userId) {
		HashMap<Integer, Float> neighborsSimilarity = new HashMap<Integer, Float>();
		int row = rowIndex.get(userId);
		for (int j = 0; j < this.columnLength; j++) {
			float similarity = CorrelationMatrix[row][j];
			if (similarity > 0) {
				neighborsSimilarity
						.put(this.GlobalNeighbors.get(j), similarity);
			}

		}

		neighborsSimilarity = (HashMap<Integer, Float>) ListOperations
				.sortByValuesDescending(neighborsSimilarity);

		return new ArrayList<Integer>(neighborsSimilarity.keySet());
	}

}
