package my;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import classic.ClassicKNN;
import util.ListOperations;
import util.MyRatings;
import base.IRecommender;
import base.MyBaseLinePredictor;
import base.Recommender;
import correlation.ICorrelationMatrix;
import correlation.PearsonFunction;

public class MyRecommender extends Recommender {

	public int GLOBAL_NEIGHBORS_PERCENTUAL = -1;

	public ArrayList<Integer> GlobalNeighbors;
	public GNCorrelationMatrix gnCorrelation;
	public NormalCorrelationMatrix otherCorrelation;
	public ICorrelationMatrix correlationMatrixType;

	public double heuristicTime;

	public MyRecommender(int percentage) {
		this.GLOBAL_NEIGHBORS_PERCENTUAL = percentage;
		setRecommenderName("MY");

	}

	public void TradicionalTrain() {
		gnCorrelation = new GNCorrelationMatrix(pearson, this.GlobalNeighbors);
		gnCorrelation.fillCorrelationMatrix();
	}

	public void GNTrain() {
		ArrayList<Integer> otherUsers = (ArrayList<Integer>) ListOperations
				.subtract(myRatings.AllUsers, this.GlobalNeighbors);

		otherCorrelation = new NormalCorrelationMatrix(pearson,
				GlobalNeighbors, otherUsers);
		gnCorrelation = new GNCorrelationMatrix(pearson, this.GlobalNeighbors);

		otherCorrelation.fillCorrelationMatrix();
		gnCorrelation.fillCorrelationMatrix();

	}

	@Override
	public void Train() throws Exception {
		super.Train();

		heuristicTime = 0;
		long startTime = System.currentTimeMillis();
		 GlobalNeighbors = createGlobalNeighbors(GLOBAL_NEIGHBORS_PERCENTUAL);
		// GlobalNeighbors =
		// createGlobalNeighborsBySimilarity(GLOBAL_NEIGHBORS_PERCENTUAL);
		// GlobalNeighbors =
		// createGlobalNeighborsByConnections(GLOBAL_NEIGHBORS_PERCENTUAL);
		//GlobalNeighbors = createGlobalNeighborsByMostRecurrent(GLOBAL_NEIGHBORS_PERCENTUAL);
		// GlobalNeighbors =
		// createGlobalNeighborsByDistinctItems(GLOBAL_NEIGHBORS_PERCENTUAL);
		// GlobalNeighbors =
		// createGlobalNeighborsByRandom(GLOBAL_NEIGHBORS_PERCENTUAL);
		long endTime = System.currentTimeMillis();
		heuristicTime = (endTime - startTime) / 1000.0;

		System.out.println("HeuristicTime: " + heuristicTime);
		System.out.println("Global Neighbors Size: " + GlobalNeighbors.size());

		if (GlobalNeighbors.size() < myRatings.AllUsers.size()) {
			GNTrain();
		} else {
			TradicionalTrain();
		}

	}

	public int getNeighborId(List neighborsList, int index) {
		return (int) neighborsList.get(index);
	}

	public double Predict(int user_id, int item_id) throws IOException,
			Exception {
		return fastPredict(user_id, item_id);
	}

	public double validationPredict(int user_id, int item_id)
			throws IOException, Exception {

		if (myRatings.userItemData.containsKey(user_id)) {
			if (GlobalNeighbors.contains(user_id)) {
				correlationMatrixType = gnCorrelation;
			} else {
				correlationMatrixType = otherCorrelation;
			}

			ArrayList<Integer> neighborList = correlationMatrixType
					.GetPositivelyCorrelatedNeighbors(user_id);
			return super.Prediction(user_id, item_id, neighborList);

		} else {
			return myBaseline.Predict(user_id, item_id);
		}

	}

	public double fastPredict(int user_id, int item_id) throws IOException,
			Exception {
		float result = myBaseline.Predict(user_id, item_id);

		if (myRatings.userItemData.containsKey(user_id)) {
			if (GlobalNeighbors.contains(user_id)) {
				correlationMatrixType = gnCorrelation;
			} else {
				correlationMatrixType = otherCorrelation;
			}
		}

		if (myRatings.userItemData.containsKey(user_id)) {

			ArrayList<Integer> correlated_users = correlationMatrixType
					.GetPositivelyCorrelatedNeighbors(user_id);

			double sum = 0;
			double weight_sum = 0;
			int neighbors = K;
			for (int user_id2 : correlated_users) {
				if (myRatings.hasRating(user_id2, item_id)) {
					double rating = myRatings.getRating(user_id2, item_id);
					double weight = correlationMatrixType.getCorrelation(
							user_id, user_id2);
					weight_sum += weight;
					sum += weight
							* (rating - myBaseline.Predict(user_id2, item_id));

					if (--neighbors == 0)
						break;
				}
			}

			if (weight_sum != 0)
				result += (sum / weight_sum);

			if (result > myRatings.MaxRating)
				result = myRatings.MaxRating;
			if (result < myRatings.MinRating)
				result = myRatings.MinRating;

			result = Math.round(result);
		}

		return result;
	}

	public ArrayList<Integer> createGlobalNeighbors(int percentage) {
		int neighborsCount = (myRatings.AllUsers.size() * percentage) / 100;
		HashMap<Integer, Integer> NumberOfItemsRatedDic = myRatings.NumberOfItemsRated;

		NumberOfItemsRatedDic = (HashMap<Integer, Integer>) ListOperations
				.sortByValuesDescending(NumberOfItemsRatedDic);

		ArrayList<Integer> globalNeighborsId = new ArrayList<Integer>(
				NumberOfItemsRatedDic.keySet());

		return new ArrayList<Integer>(globalNeighborsId.subList(0,
				neighborsCount));
	}

	public ArrayList<Integer> createGlobalNeighborsByRandom(int percentage) {
		int neighborsCount = (myRatings.AllUsers.size() * percentage) / 100;

		ArrayList<Integer> globalNeighborsId = (ArrayList<Integer>) myRatings.AllUsers;
		Collections.shuffle(globalNeighborsId);

		return new ArrayList<Integer>(globalNeighborsId.subList(0,
				neighborsCount));
	}

	public ArrayList<Integer> createGlobalNeighborsByDistinctItems(
			int percentage) {
		int neighborsCount = (myRatings.AllUsers.size() * percentage) / 100;
		HashMap<Integer, Integer> NumberOfItemsRatedDic = myRatings.NumberOfItemsRated;

		NumberOfItemsRatedDic = (HashMap<Integer, Integer>) ListOperations
				.sortByValuesDescending(NumberOfItemsRatedDic);

		ArrayList<Integer> usersId = new ArrayList<Integer>(
				NumberOfItemsRatedDic.keySet());

		ArrayList<Integer> distinctItemsList = new ArrayList<Integer>();
		ArrayList<Integer> globalNeighbors = new ArrayList<Integer>();
		globalNeighbors.add(usersId.get(0));
		distinctItemsList = (ArrayList<Integer>) myRatings.getItems(usersId
				.get(0));
		for (int i = 1; i < usersId.size(); i++) {

			ArrayList<Integer> itemList = (ArrayList<Integer>) myRatings
					.getItems(usersId.get(i));
			ArrayList<Integer> aux = new ArrayList<Integer>(distinctItemsList);

			Set<Integer> set = new HashSet<>(itemList);
			set.addAll(aux);
			ArrayList<Integer> mergeList = new ArrayList<Integer>(set);

			if (mergeList.size() > distinctItemsList.size()) {
				globalNeighbors.add(usersId.get(i));
				distinctItemsList = new ArrayList<Integer>(mergeList);
			}

			if (globalNeighbors.size() >= neighborsCount) {
				break;
			}
		}

		int i = 0;
		while (i <= usersId.size() && globalNeighbors.size() < neighborsCount) {
			if (!globalNeighbors.contains(usersId.get(i))) {
				globalNeighbors.add(usersId.get(i));
			}
			i++;

		}

		return globalNeighbors;
	}

	public ArrayList<Integer> createGlobalNeighborsBySimilarity(int percentage) {
		int neighborsCount = (myRatings.AllUsers.size() * percentage) / 100;
		HashMap<Integer, Float> similarityAverage = new HashMap<Integer, Float>();

		ClassicKNN knn = classicKnn();

		for (int i = 0; i < myRatings.AllUsers.size(); i++) {
			int userId1 = myRatings.AllUsers.get(i);
			float mean = 0;
			for (int j = 0; j < myRatings.AllUsers.size(); j++) {
				if (i != j) {
					int userId2 = myRatings.AllUsers.get(j);
					float similarity = knn.correlationMatrix.getCorrelation(
							userId1, userId2);
					mean += similarity;
				}
			}
			float finalSimilarity = mean / myRatings.AllUsers.size() - 1;
			similarityAverage.put(userId1, finalSimilarity);
		}

		similarityAverage = (HashMap<Integer, Float>) ListOperations
				.sortByValuesDescending(similarityAverage);

		ArrayList<Integer> globalNeighborsId = new ArrayList<Integer>(
				similarityAverage.keySet());

		return new ArrayList<Integer>(globalNeighborsId.subList(0,
				neighborsCount));
	}
	

	public ArrayList<Integer> createGlobalNeighborsByConnections(int percentage) {
		int neighborsCount = (myRatings.AllUsers.size() * percentage) / 100;
		HashMap<Integer, Integer> numberOfconnections = new HashMap<Integer, Integer>();

		ClassicKNN knn = classicKnn();

		for (int i = 0; i < myRatings.AllUsers.size(); i++) {
			int userId1 = myRatings.AllUsers.get(i);
			int connections = 0;
			for (int j = 0; j < myRatings.AllUsers.size(); j++) {
				if (i != j) {
					int userId2 = myRatings.AllUsers.get(j);
					float similarity = knn.correlationMatrix.getCorrelation(
							userId1, userId2);
					if (similarity > 0) {
						connections++;
					}

				}
			}
			numberOfconnections.put(userId1, connections);
		}

		numberOfconnections = (HashMap<Integer, Integer>) ListOperations
				.sortByValuesDescending(numberOfconnections);

		ArrayList<Integer> globalNeighborsId = new ArrayList<Integer>(
				numberOfconnections.keySet());

		return new ArrayList<Integer>(globalNeighborsId.subList(0,
				neighborsCount));
	}

	public ArrayList<Integer> createGlobalNeighborsByMostRecurrent(
			int percentage) {
		int neighborsCount = (myRatings.AllUsers.size() * percentage) / 100;
		HashMap<Integer, Integer> recurrentList = new HashMap<Integer, Integer>();

		ClassicKNN knn = classicKnn();

		for (int i = 0; i < myRatings.AllUsers.size(); i++) {
			int userId1 = myRatings.AllUsers.get(i);
			List<Integer> correlated_users = knn.correlationMatrix
					.GetPositivelyCorrelatedNeighbors(userId1);
			int neighbors = K;

			for (int j = 0; j < correlated_users.size(); j++) {

				int userId2 = correlated_users.get(j);
				if (!recurrentList.containsKey(userId2)) {
					recurrentList.put(userId2, 0);
				}
				int value = recurrentList.get(userId2) + 1;
				recurrentList.put(userId2, value);

				if (--neighbors == -0)
					break;
			}
		}

		recurrentList = (HashMap<Integer, Integer>) ListOperations
				.sortByValuesDescending(recurrentList);

		ArrayList<Integer> globalNeighborsId = new ArrayList<Integer>(
				recurrentList.keySet());

		return new ArrayList<Integer>(globalNeighborsId.subList(0,
				neighborsCount));
	}

	public ClassicKNN classicKnn() {
		ClassicKNN knn = new ClassicKNN();
		knn.setData(myRatings, "1m");
		knn.setK(this.K);
		knn.setRecommenderName(this.recommenderName);
		knn.setShrinkage(this.Shrinkage);
		try {
			knn.Train();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return knn;
	}

}
