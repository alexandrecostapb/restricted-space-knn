package my;

import java.util.ArrayList;
import java.util.HashMap;

import util.ListOperations;
import util.MyRatings;
import correlation.ICorrelationMatrix;
import correlation.PearsonFunction;

public class GNCorrelationMatrix implements ICorrelationMatrix{
	public float[][] CorrelationMatrix;
	public int matrixSize;
	public PearsonFunction pearsonCorrelation;
	public HashMap<Integer, Integer> IndexMapping;
	public MyRatings myRatings;
	public ArrayList<Integer> GlobalNeighbors;

	public GNCorrelationMatrix(PearsonFunction correlationFunction,
			ArrayList<Integer> globalNeighbors) {
		this.GlobalNeighbors = globalNeighbors;
		this.matrixSize = globalNeighbors.size();
		this.pearsonCorrelation = correlationFunction;
	     createIndexMapping();

	}

	public void createIndexMapping() {
		IndexMapping = new HashMap<Integer, Integer>();
		for (int i = 0; i < this.matrixSize; i++) {
			IndexMapping.put(this.GlobalNeighbors.get(i), i);
		}
	}

	public void fillCorrelationMatrix() {
		this.CorrelationMatrix = new float[this.matrixSize][];
		for (int i = 0; i < this.matrixSize; i++) {
			this.CorrelationMatrix[i] = new float[i + 1];
			for (int j = 0; j < this.CorrelationMatrix[i].length; j++) {
				if (i == j) {
					this.CorrelationMatrix[i][j] = 1;
				} else {
					this.CorrelationMatrix[i][j] = pearsonCorrelation
							.computeCorrelation(GlobalNeighbors.get(i),
									GlobalNeighbors.get(j));
				}
			}
		}
	}

	public float getCorrelation(int userId1, int userId2) {
		int rowIndex = IndexMapping.get(userId1);
		int columnIndex = IndexMapping.get(userId2);
		if (columnIndex > rowIndex) {
			return CorrelationMatrix[columnIndex][rowIndex];
		}

		return CorrelationMatrix[rowIndex][columnIndex];
	}

	public ArrayList<Integer> GetPositivelyCorrelatedNeighbors(int userId) {
		HashMap<Integer, Float> neighborsSimilarity = new HashMap<Integer, Float>();
		int rowIndex = IndexMapping.get(userId);
		for (int j = 0; j < this.matrixSize; j++) {
			float similarity;
			if (j <= rowIndex) {
				similarity = CorrelationMatrix[rowIndex][j];
			} else {
				similarity = CorrelationMatrix[j][rowIndex];
			}
			if (similarity > 0) {
				neighborsSimilarity
						.put(this.GlobalNeighbors.get(j), similarity);
			}
		}

		neighborsSimilarity = (HashMap<Integer, Float>) ListOperations
				.sortByValuesDescending(neighborsSimilarity);

		return new ArrayList<Integer>(neighborsSimilarity.keySet());
	}

}
