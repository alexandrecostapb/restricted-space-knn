package gn;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import util.DataSaver;
import util.MyRatings;
import util.Tuple;
import base.IRecommender;
import base.MyBaseLinePredictor;
import base.Recommender;
import correlation.CorrelationMatrix;
import correlation.PearsonFunction;

public class GNRecommender extends Recommender{
	public CorrelationMatrix myCorrelation;
	public int K;
	public MyRatings myRatings;
	public MyRatings trainningData;
	public List<Integer> GlobalNeighbors;
	public MyBaseLinePredictor myBaseline;
	public float Shrinkage;
	public String databaseName;
	public PearsonFunction pearson;
	public final String RECOMMENDER_NAME = "GN";

	public int gnSize;
	public String folderName;
	public int folderNumber;

	public GNRecommender() {
	}

	public void setData(MyRatings myRatings, String dataBaseName) {
		this.myRatings = myRatings;
		this.databaseName = dataBaseName;

	}

	public String getRecommenderName() {
		return RECOMMENDER_NAME;
	}

	public float getShrinkage() {
		return Shrinkage;
	}

	public void setShrinkage(float shrinkage) {
		Shrinkage = shrinkage;
	}

	public void setK(int k) {
		this.K = k;
	}

	public int getK() {
		return this.K;
	}
	
	public void test(){
		String trainDataPath = "datasets/" + folderName + "/" + folderNumber
				+ "/validation/train_" + this.databaseName + ".dat";

		String validationDataPath = "datasets/" + folderName + "/"
				+ folderNumber + "/validation/validation_"
				+ this.databaseName + ".dat";
		
		File file = new File("datasets/" + folderName + "/" + folderNumber
				+ "/validation/");
		if (!file.exists()) {
			file.mkdir();
			
		}
		try {
			splitDataForValidation(myRatings, trainDataPath, validationDataPath);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void Train() throws Exception {
		myBaseline = new MyBaseLinePredictor(myRatings);
		myBaseline.Train();
		
		String trainDataPath = "datasets/" + folderName + "/" + folderNumber
				+ "/validation/train_" + this.databaseName + ".dat";

		String validationDataPath = "datasets/" + folderName + "/"
				+ folderNumber + "/validation/validation_"
				+ this.databaseName + ".dat";
		
		File file = new File("datasets/" + folderName + "/" + folderNumber
				+ "/validation/");
		if (!file.exists()) {
			file.mkdir();
		}
		//splitDataForValidation(myRatings, trainDataPath, validationDataPath);
		trainningData = new MyRatings();
		trainningData.createData(trainDataPath);

		MyRatings validationData = new MyRatings();
		validationData.createData(validationDataPath);

		pearson = new PearsonFunction();
		pearson.setData(myRatings, this.Shrinkage);

		myCorrelation = new CorrelationMatrix(pearson, trainningData.AllUsers);
		myCorrelation.fillCorrelationMatrix();

		StochastiSearch sls = new StochastiSearch();
		sls.setData(trainningData, validationData, myCorrelation, myBaseline, K);
		this.GlobalNeighbors = sls.findGlobalNeighbors();
		this.gnSize = this.GlobalNeighbors.size();

	}

	public void splitDataForValidation(MyRatings myRatings, String trainPath,
			String validationPath) throws IOException {
		List<Tuple> data = new ArrayList<Tuple>();

		for (Map.Entry<Integer, HashMap<Integer, Float>> userData : myRatings.userItemData
				.entrySet()) {
			{
				int userId = userData.getKey();

				for (Map.Entry<Integer, Float> item : userData.getValue()
						.entrySet()) {

					int itemId = item.getKey();
					float rating = item.getValue();
					data.add(new Tuple(userId, itemId, rating));
				}
			}
			Collections.shuffle(data);

			float trainDataLenght = 0.85F;
			float validationDataLengh = 1 - trainDataLenght;

			StringBuilder trainData = new StringBuilder();
			StringBuilder validationData = new StringBuilder();

			for (int i = 0; i < data.size() * trainDataLenght; i++) {
				trainData.append(data.get(i).getUserId());
				trainData.append(" ");
				trainData.append(data.get(i).getItemId());
				trainData.append(" ");
				trainData.append(data.get(i).getRating());
				trainData.append("\n");
			}

			for (int i = (int) (data.size() * trainDataLenght); i < data.size(); i++) {
				validationData.append(data.get(i).getUserId());
				validationData.append(" ");
				validationData.append(data.get(i).getItemId());
				validationData.append(" ");
				validationData.append(data.get(i).getRating());
				validationData.append("\n");
			}

			DataSaver.save(trainData.toString(), trainPath);
			DataSaver.save(validationData.toString(), validationPath);
		}

	}

	public double Predict(int user_id, int item_id) throws IOException,
			Exception {
		return MyPrediction(user_id, item_id);
	}

	public double MyPrediction(int user_id, int item_id) throws IOException,
			Exception {
		double result = myBaseline.Predict(user_id, item_id);

		if (trainningData.userItemData.containsKey(user_id)) {
			ArrayList<Integer> correlated_users = myCorrelation
					.GetPositivelyCorrelatedNeighbors(user_id, GlobalNeighbors);

			double sum = 0;
			double weight_sum = 0;
			int neighbors = K;
			for (int user_id2 : correlated_users) {
				if (trainningData.hasRating(user_id2, item_id)) {
					double rating = trainningData.getRating(user_id2, item_id);
					double weight = pearson.computeCorrelation(user_id,
							user_id2);
					weight_sum += weight;
					sum += weight
							* (rating - myBaseline.Predict(user_id2, item_id));

					if (--neighbors == 0)
						break;
				}
			}

			if (weight_sum != 0)
				result += (float) (sum / weight_sum);

			if (result > trainningData.MaxRating)
				result = trainningData.MaxRating;
			if (result < trainningData.MinRating)
				result = trainningData.MinRating;

			result = Math.round(result);
		}

		return result;
	}

	@Override
	public int getNeighborId(List neighborsList, int index) {
		return (int) neighborsList.get(index);
	}
}
