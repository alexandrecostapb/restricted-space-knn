package gn;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import util.ListOperations;
import util.MyRatings;
import base.MyBaseLinePredictor;
import correlation.CorrelationMatrix;

public class StochastiSearch {
	public float beta;
	public int ps;
	public float psMax;
	public float pActive;
	public MyRatings trainningData;
	public MyRatings validationData;
	public int n;
	public ArrayList<ArrayList<Character>> history;
	public int historyMaxSize;
	public float pWorser;
	public int N_Sample;
	public int N_Radius;
	public Random random;
	public float alpha;
	public int K;

	public CorrelationMatrix correlationMatrix;
	public List<Integer> GlobalNeighbors;
	public MyBaseLinePredictor myBaseline;

	public StochastiSearch() {
		history = new ArrayList<ArrayList<Character>>();
		this.pActive = 0.15F;
		this.historyMaxSize = 50;
		this.beta = 1.01F;
		this.N_Sample = 10;
		this.N_Radius = 1;
		this.psMax = n * pActive;
		this.pWorser = 0.1F;
		this.ps = 0;
		this.alpha = 0.65F;
		random = new Random();
	}

	public void setData(MyRatings trainningData, MyRatings validationData,
			CorrelationMatrix correlationMatrix,
			MyBaseLinePredictor myBaseline, int k) {
		this.trainningData = trainningData;
		this.n = trainningData.AllUsers.size();
		this.validationData = validationData;
		this.myBaseline = myBaseline;
		this.correlationMatrix = correlationMatrix;
		this.K = k;
	}

	public List<Integer> findGlobalNeighbors() throws Exception {
		List<Character> gnCharacterList = IteratedLocalSearch(50);
		this.GlobalNeighbors = mappingUserIds(gnCharacterList);
		System.out.println("BEST SOLUTION FITNESS: "
				+ countOnes(gnCharacterList));
		//this.GlobalNeighbors =  getUserConnectionList(correlationMatrix, 152);
		return this.GlobalNeighbors;
	}

	public List<Character> IteratedLocalSearch(int max_iter) throws Exception {
		long startTime;
		long endTime;
		List<Character> x = GenInitialSol();
		x = LocalSearch(x);
		List<Character> x_hat = copyList(x);
		int stuck = 0;
		startTime = System.currentTimeMillis();
		while (stuck <= max_iter) {
			
			ArrayList<Character> x_line = Pertub(x, ps);
			x_line = LocalSearch(x_line);
			if (Fitness(x_line) < Fitness(x) && !InHist(x_line)) {
				AddToHist(x_line); // better and unseen solution
				x = copyList(x_line);
				ps = Max(2, (1 / beta) * ps);

				if (Fitness(x_line) < Fitness(x_hat)) {
					x_hat = copyList(x_line);
					stuck = 0;
				}
			} else {
				stuck++;
				ps = Max(beta * ps, psMax);
				// float number = randomFloat(0, 1);
				// if (pWorser > number)
				// {
				// x = copyList(x_line); //keep worse solution
				// }
			}
			endTime = System.currentTimeMillis();
			double time = (endTime - startTime) / 1000.0;
			if (time > 60) {
				break;
			}

		}
		return x_hat;
	}

	public ArrayList<Character> LocalSearch(List<Character> list)
			throws Exception {
		ArrayList<Character> x = copyList(list);
		boolean found_better = true;
		while (found_better) {
			found_better = false;
			List<Character> t = argMax(x, this.N_Sample, this.N_Radius);
			if (Fitness(t) < Fitness(x)) {
				found_better = true;
				x = copyList(t);
			}
		}
		return x;
	}

	public ArrayList<Character> copyList(List<Character> x) {
	
		return new ArrayList<Character>(x);
	}

	public List<Character> argMax(List<Character> list, int N_Sample,
			int N_Radius) throws Exception {
		List<Character> x = copyList(list);
		HashMap<Integer, Float> fitnessArrays = new HashMap<Integer, Float>();
		List<List<Character>> flippedLists = new ArrayList<List<Character>>();

		for (int i = 0; i < N_Sample; i++) {
			List<Character> flipped = Pertub(x, N_Radius);
			flippedLists.add(flipped);
			fitnessArrays.put(i, Fitness(flippedLists.get(i)));

		}

		fitnessArrays = (HashMap<Integer, Float>) ListOperations
				.sortByValuesAscending(fitnessArrays);
		int index = (int) fitnessArrays.keySet().toArray()[0];

		return flippedLists.get(index);
	}

	public float randomFloat(int minValue, int maxValue) {
		return (float) (random.nextDouble() * (maxValue - minValue) + minValue);

	}

	public int randomInt(int minValue, int maxValue) {
		return random.nextInt((maxValue - minValue) + 1) + minValue;
	}

	public List<Character> GenRandomInitialSol() {
		int gnSize = (int) (n * pActive);
		
		List<Character> x = new ArrayList<Character>();
		for (int i = 0; i < n; i++) {
			x.add('0');
		}

		ArrayList<Integer> randomIndexes = createRandomList(gnSize, 0, n - 1);
		for (int i = 0; i < randomIndexes.size(); i++) {
			x.set(randomIndexes.get(i), '1');
		}
		return x;
	}

	public List<Character> GenInitialSol() {
		int gnSize = (int) (n * pActive);
		List<Character> x = new ArrayList<Character>();
		List<Integer> list = getUserConnectionList(correlationMatrix,
				gnSize);
		for (int i = 0; i < trainningData.AllUsers.size(); i++) {
			if (list.contains(trainningData.AllUsers.get(i))) {
				x.add('1');
			} else {
				x.add('0');
			}
		}

		return x;
	}

	public boolean isListEqual(List<Character> x1, List<Character> x2) {

		if (x1.size() != x2.size()) {
			return false;
		}
		for (int i = 0; i < x1.size(); i++) {
			if (!x1.get(i).equals(x2.get(i))) {
				return false;
			}
		}
		return true;

	}

	public float Fitness2(List<Character> x) {
		return countOnes(x);
	}

	public float Fitness(List<Character> x) throws Exception {
		this.GlobalNeighbors = mappingUserIds(x);
		float fitness = (alpha * fitnessMAE(validationData))
				+ ((1.0F - alpha) * (this.GlobalNeighbors.size() * 100 / this.trainningData.AllUsers
						.size()));

		return fitness;
	}

	public ArrayList<Integer> mappingUserIds(List<Character> x) {
		ArrayList<Integer> ids = new ArrayList<Integer>();
		for (int i = 0; i < x.size(); i++) {
			if (x.get(i).equals('1')) {
				ids.add(trainningData.AllUsers.get(i));
			}
		}
		return ids;
	}

	public float fitnessPrediction(int user_id, int item_id) throws Exception {
		float result = myBaseline.Predict(user_id, item_id);

		if (trainningData.userItemData.containsKey(user_id)) {
			List<Integer> correlated_users = correlationMatrix
					.GetPositivelyCorrelatedNeighbors(user_id, GlobalNeighbors);
			double sum = 0;
			double weight_sum = 0;
			int neighbors = K;
			for (int user_id2 : correlated_users) {
				if (trainningData.hasRating(user_id2, item_id)) {
					float rating = trainningData.getRating(user_id2, item_id);
					float weight = correlationMatrix.getCorrelation(user_id,
							user_id2);
					weight_sum += weight;
					sum += weight
							* (rating - myBaseline.Predict(user_id2, item_id));

					if (--neighbors == 0)
						break;
				}
			}

			if (weight_sum != 0)
				result += (float) (sum / weight_sum);

			if (result > trainningData.MaxRating)
				result = trainningData.MaxRating;
			if (result < trainningData.MinRating)
				result = trainningData.MinRating;

			result = (float) Math.round(result);
		}

		return result;
	}

	public float fitnessMAE(MyRatings testData) throws Exception {
		float sum = 0;
		int count = 0;
		for (int i = 0; i < testData.AllUsers.size(); i++) {
			int userId = testData.AllUsers.get(i);
			HashMap<Integer, Float> itemsAndRatings = testData
					.getItemsAndRatings(userId);
			for (Map.Entry<Integer, Float> item : itemsAndRatings.entrySet()) {
				count++;
				double prediction = fitnessPrediction(userId, item.getKey());
				double realRating = item.getValue();
				sum += Math.abs(prediction - realRating);
			}
		}

		return sum / count;
	}

	public List<Integer> getUserConnectionList(
			CorrelationMatrix correlationMatrix, int quantity) {
		HashMap<Integer, Integer> userConnectionDic = createUserConnectionDic(correlationMatrix);
		ArrayList<Integer> list = new ArrayList<Integer>(
				userConnectionDic.keySet());

		return list.subList(0, quantity);

	}

	public HashMap<Integer, Integer> createUserConnectionDic(
			CorrelationMatrix correlationMatrix) {
		HashMap<Integer, Integer> userConnectionDic = new HashMap<Integer, Integer>();
		for (int i = 0; i < trainningData.AllUsers.size(); i++) {
			int userId1 = trainningData.AllUsers.get(i);
			int connectionNumber = 0;
			for (int j = 0; j < trainningData.AllUsers.size(); j++) {
				int userId2 = trainningData.AllUsers.get(j);
				if (i != j
						&& correlationMatrix.getCorrelation(userId1, userId2) > 0) {
					connectionNumber++;
				}
			}
			userConnectionDic.put(userId1, connectionNumber);
		}

		userConnectionDic = (HashMap<Integer, Integer>) ListOperations
				.sortByValuesDescending(userConnectionDic);

		return userConnectionDic;
	}

	public boolean InHist(List<Character> x) {
		for (int i = 0; i < history.size(); i++) {
			if (isListEqual(history.get(i), x)) {
				return true;
			}
		}
		return false;
	}

	public void AddToHist(ArrayList<Character> x) {
		if (history.size() >= historyMaxSize) {
			history.remove(0);
		}
		history.add(x);
	}

	public ArrayList<Character> Pertub(List<Character> x, int ps) {
		ArrayList<Character> aux = copyList(x);
		if (ps > aux.size()) {
			ps = aux.size();
		}
		ArrayList<Integer> indexesToBePertubed = createRandomList(ps, 0,
				aux.size() - 1);

		for (int i = 0; i < indexesToBePertubed.size(); i++) {
			int index = indexesToBePertubed.get(i);

			if (aux.get(index).equals('0'))
				aux.set(index, '1');
			else
				aux.set(index, '0');
		}

		return aux;
	}

	public int Max(float n1, float n2) {
		if (n1 > n2) {
			return (int) n1;
		}
		return (int) n2;
	}

	public String printX(List<Character> x) {
		String print = "[";
		for (int i = 0; i < x.size(); i++) {
			print = print + x.get(i) + " ";
		}
		print = print + "]";
		return print;
	}

	public int countOnes(List<Character> x) {
		int sum = 0;
		for (int i = 0; i < x.size(); i++) {
			if (x.get(i).equals('1')) {
				sum++;
			}
		}
		return sum;
	}

	public int countZeros(List<Character> x) {
		return x.size() - countOnes(x);
	}

	public ArrayList<Integer> createRandomList(int indexListSize, int minIndex,
			int maxIndex) {
		ArrayList<Integer> randomValues = new ArrayList<Integer>();
		while (randomValues.size() < indexListSize) {
			int number = randomInt(minIndex, maxIndex);
			if (!randomValues.contains(number)) {
				randomValues.add(number);
			}
		}
		return randomValues;
	}

}
