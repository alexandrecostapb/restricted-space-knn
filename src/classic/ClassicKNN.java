package classic;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import util.MyRatings;
import base.MyBaseLinePredictor;
import base.Recommender;
import correlation.CorrelationMatrix;
import correlation.PearsonFunction;

public class ClassicKNN extends Recommender {

	public CorrelationMatrix correlationMatrix;

	public ClassicKNN() {
		setRecommenderName("CLASSIC");
	}

	@Override
	public void Train() throws Exception {

		super.Train();
		correlationMatrix = new CorrelationMatrix(pearson, myRatings.AllUsers);
		correlationMatrix.fillCorrelationMatrix();

	}

	public double Predict(int user_id, int item_id) throws Exception {
		return fastPredict(user_id, item_id);
	}

	@Override
	public int getNeighborId(List neighborsList, int index) {
		return (int) neighborsList.get(index);
	}

	public double validationPredict(int user_id, int item_id) throws Exception {

		if (myRatings.userItemData.containsKey(user_id)) {
			ArrayList<Integer> neighborList = correlationMatrix
					.GetPositivelyCorrelatedNeighbors(user_id);
			return super.Prediction(user_id, item_id, neighborList);
		} else {
			return myBaseline.Predict(user_id, item_id);
		}

	}

	public double fastPredict(int user_id, int item_id) throws Exception {
		double result = myBaseline.Predict(user_id, item_id);

		if (myRatings.userItemData.containsKey(user_id)) {

			ArrayList<Integer> correlated_users = correlationMatrix
					.GetPositivelyCorrelatedNeighbors(user_id);

			double sum = 0;
			double weight_sum = 0;
			int neighbors = K;
			for (int i = 0; i < correlated_users.size(); i++) {

				int user_id2 = correlated_users.get(i);
				if (myRatings.hasRating(user_id2, item_id)) {
					double rating = myRatings.getRating(user_id2, item_id);
					double weight = correlationMatrix.getCorrelation(user_id,
							user_id2);
					weight_sum = weight_sum + weight;
					sum = sum + weight
							* (rating - myBaseline.Predict(user_id2, item_id));

					if (--neighbors == -0)
						break;
				}
			}

			if (weight_sum != 0)
				result += (sum / weight_sum);

			if (result > myRatings.MaxRating)
				result = myRatings.MaxRating;
			if (result < myRatings.MinRating)
				result = myRatings.MinRating;

			result = (float) Math.round(result);
		}

		return result;
	}

}
