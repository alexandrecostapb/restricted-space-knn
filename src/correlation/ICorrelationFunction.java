package correlation;

public interface ICorrelationFunction {
	 float computeCorrelation(int userId, int otherId);

}
