package correlation;

import java.util.ArrayList;

public interface ICorrelationMatrix {

	
	public void fillCorrelationMatrix();
	
	public float getCorrelation(int userId1, int userId2);
	
	public ArrayList<Integer> GetPositivelyCorrelatedNeighbors(int userId);
}
