package correlation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import util.MyRatings;
import util.SimilarityComparator;
import util.Test;

public class CosineFunction implements ICorrelationFunction {

	public MyRatings myRatings;
	public float Shrinkage;

	public void setData(MyRatings myRatings, float shrinkage) {
		this.myRatings = myRatings;
		this.Shrinkage = shrinkage;
	}

	public float computeCorrelation(int userId_x, int userId_y) {
		int n = 0;
		float xy_sum = 0;
		float xx_sum = 0;
		float yy_sum = 0;

		HashMap<Integer, Float> x_values = myRatings
				.getItemsAndRatings(userId_x);
		HashMap<Integer, Float> y_values = myRatings
				.getItemsAndRatings(userId_y);

		double dotProduct = 0.0;
		double normA = 0.0;
		double normB = 0.0;

		for (int item : x_values.keySet()) {

			if (y_values.containsKey(item)) {
				n++;
				float value_x = x_values.get(item);
				float value_y = y_values.get(item);

				dotProduct += value_x * value_y;
				normA += Math.pow(value_x, 2);
				normB += Math.pow(value_y, 2);

			}
		}

		if (n < 2)
			return 0;

		float numerator = (float) (xy_sum);
		float denominator = (float) Math.sqrt(xx_sum) * (yy_sum);

		if (denominator == 0)
			return 0;

		float correlation = (numerator / denominator);

		// correlation = correlation * ((n - 1) / (n - 1 + Shrinkage));

		correlation = (float) (dotProduct / (Math.sqrt(normA) * Math
				.sqrt(normB)));

		correlation = correlation * ((n - 1) / (n - 1 + Shrinkage));

		return correlation;
	}

	public ArrayList<Test> GetPositivelyCorrelatedNeighbors(int userId,
			List<Integer> userIsList) {
		ArrayList<Test> similarityArray = new ArrayList<Test>();

		for (int i = 0; i < userIsList.size(); i++) {
			int otherId = userIsList.get(i);
			float similarity = computeCorrelation(userId, otherId);
			if (similarity > 0) {
				Test t = new Test();
				t.setId(otherId);
				t.setSimmilarity(similarity);
				similarityArray.add(t);
			}
		}

		Collections.sort(similarityArray, new SimilarityComparator());

		return similarityArray;
	}

	public float computeCorrelationFromDenseVectors(float[] vector_x,
			float[] vector_y) {
		int n = vector_x.length;
		float x_sum = 0;
		float y_sum = 0;
		float xy_sum = 0;
		float xx_sum = 0;
		float yy_sum = 0;

		for (int i = 0; i < vector_x.length; i++) {
			float value_x = vector_x[i];
			float value_y = vector_y[i];
			x_sum += value_x;
			y_sum += value_y;
			xy_sum += value_x * value_y;
			xx_sum += value_x * value_x;
			yy_sum += value_y * value_y;
		}

		float numerator = (float) (n * xy_sum - x_sum * y_sum);
		float denominator = (float) Math.sqrt((n * xx_sum - x_sum * x_sum)
				* (n * yy_sum - y_sum * y_sum));

		if (denominator == 0)
			return 0;

		float correlation = (numerator / denominator);

		correlation = correlation * ((n - 1) / (n - 1 + Shrinkage));

		return (float) correlation;
	}

}
