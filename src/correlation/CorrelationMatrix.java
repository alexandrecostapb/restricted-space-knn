package correlation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import util.ListOperations;

public class CorrelationMatrix {
	public float[][] Matrix;
	public List<Integer> AllUsers;
	public int matrixSize;
	public HashMap<Integer, Integer> IndexMapping;
	public PearsonFunction CorrelationFunction;

	public CorrelationMatrix(PearsonFunction correlationFunction,
			List<Integer> usersId) {
		this.AllUsers = usersId;
		this.matrixSize = AllUsers.size();
		this.CorrelationFunction = correlationFunction;
		this.Matrix = new float[this.matrixSize][this.matrixSize];
		createIndexMapping();

	}

	public void createIndexMapping() {
		IndexMapping = new HashMap<Integer, Integer>();
		for (int i = 0; i < this.matrixSize; i++) {
			IndexMapping.put(this.AllUsers.get(i), i);
		}
	}

	public void fillCorrelationMatrix() {
		for (int i = 0; i < this.matrixSize; i++) {
			for (int j = 0; j < this.matrixSize; j++) {
				if (i == j) {
					Matrix[i][j] = 1;
				} else {
					if (i < j) {
						Matrix[i][j] = CorrelationFunction.computeCorrelation(
								AllUsers.get(i), AllUsers.get(j));

					} else {
						Matrix[i][j] = Matrix[j][i];
					}
				}
			}
		}
	}

	public float getCorrelation(int userId1, int userId2) {
		return Matrix[IndexMapping.get(userId1)][IndexMapping.get(userId2)];
	}

	public ArrayList<Integer> GetPositivelyCorrelatedNeighbors(int userId) {
		HashMap<Integer, Float> neighborsSimilarity = new HashMap<Integer, Float>();
		int row = IndexMapping.get(userId);
		for (int j = 0; j < this.matrixSize; j++) {
			float similarity = Matrix[row][j];
			if (similarity > 0) {
				neighborsSimilarity.put(this.AllUsers.get(j), similarity);
			}

		}

		neighborsSimilarity = (HashMap<Integer, Float>) ListOperations
				.sortByValuesDescending(neighborsSimilarity);

		return new ArrayList<Integer>(neighborsSimilarity.keySet());
	}

	public ArrayList<Integer> GetPositivelyCorrelatedNeighbors(int userId,
			List<Integer> globalNeighbors) {
		HashMap<Integer, Float> neighborsSimilarity = new HashMap<Integer, Float>();
		for (int j = 0; j < globalNeighbors.size(); j++) {
			float similarity = getCorrelation(userId, globalNeighbors.get(j));
			if (similarity > 0) {
				neighborsSimilarity.put(globalNeighbors.get(j), similarity);
			}

		}

		neighborsSimilarity = (HashMap<Integer, Float>) ListOperations
				.sortByValuesDescending(neighborsSimilarity);

		return new ArrayList<Integer>(neighborsSimilarity.keySet());
	}

}
