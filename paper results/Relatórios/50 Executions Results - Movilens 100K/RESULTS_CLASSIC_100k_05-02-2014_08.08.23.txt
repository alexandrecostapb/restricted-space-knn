DATABASE NAME: 100k
FOLDERS: 5
DATABASES PER FOLDER: 10 of trainning and 10 of testing.
K: 30

Folder: 1 - RECOMMENDER: 100k
Folder: 1 - MAE: 0.6851
Folder: 1 - TRAIN TIME: 1.482 seconds
Folder: 1 - EVALUATION TIME: 3.339 seconds

Folder: 2 - RECOMMENDER: 100k
Folder: 2 - MAE: 0.6802
Folder: 2 - TRAIN TIME: 1.326 seconds
Folder: 2 - EVALUATION TIME: 3.058 seconds

Folder: 3 - RECOMMENDER: 100k
Folder: 3 - MAE: 0.6783
Folder: 3 - TRAIN TIME: 1.528 seconds
Folder: 3 - EVALUATION TIME: 3.027 seconds

Folder: 4 - RECOMMENDER: 100k
Folder: 4 - MAE: 0.6804
Folder: 4 - TRAIN TIME: 1.419 seconds
Folder: 4 - EVALUATION TIME: 3.042 seconds

Folder: 5 - RECOMMENDER: 100k
Folder: 5 - MAE: 0.6816
Folder: 5 - TRAIN TIME: 1.28 seconds
Folder: 5 - EVALUATION TIME: 3.026 seconds

Folder: 6 - RECOMMENDER: 100k
Folder: 6 - MAE: 0.678
Folder: 6 - TRAIN TIME: 1.263 seconds
Folder: 6 - EVALUATION TIME: 3.042 seconds

Folder: 7 - RECOMMENDER: 100k
Folder: 7 - MAE: 0.685
Folder: 7 - TRAIN TIME: 1.31 seconds
Folder: 7 - EVALUATION TIME: 3.011 seconds

Folder: 8 - RECOMMENDER: 100k
Folder: 8 - MAE: 0.6711
Folder: 8 - TRAIN TIME: 1.264 seconds
Folder: 8 - EVALUATION TIME: 3.042 seconds

Folder: 9 - RECOMMENDER: 100k
Folder: 9 - MAE: 0.6855
Folder: 9 - TRAIN TIME: 1.311 seconds
Folder: 9 - EVALUATION TIME: 3.026 seconds

Folder: 10 - RECOMMENDER: 100k
Folder: 10 - MAE: 0.6885
Folder: 10 - TRAIN TIME: 1.311 seconds
Folder: 10 - EVALUATION TIME: 3.026 seconds

Folder: 11 - RECOMMENDER: 100k
Folder: 11 - MAE: 0.6874
Folder: 11 - TRAIN TIME: 1.263 seconds
Folder: 11 - EVALUATION TIME: 3.042 seconds

Folder: 12 - RECOMMENDER: 100k
Folder: 12 - MAE: 0.6918
Folder: 12 - TRAIN TIME: 1.279 seconds
Folder: 12 - EVALUATION TIME: 3.042 seconds

Folder: 13 - RECOMMENDER: 100k
Folder: 13 - MAE: 0.6819
Folder: 13 - TRAIN TIME: 1.264 seconds
Folder: 13 - EVALUATION TIME: 3.073 seconds

Folder: 14 - RECOMMENDER: 100k
Folder: 14 - MAE: 0.6887
Folder: 14 - TRAIN TIME: 1.248 seconds
Folder: 14 - EVALUATION TIME: 3.011 seconds

Folder: 15 - RECOMMENDER: 100k
Folder: 15 - MAE: 0.6764
Folder: 15 - TRAIN TIME: 1.263 seconds
Folder: 15 - EVALUATION TIME: 3.074 seconds

Folder: 16 - RECOMMENDER: 100k
Folder: 16 - MAE: 0.6747
Folder: 16 - TRAIN TIME: 1.295 seconds
Folder: 16 - EVALUATION TIME: 3.073 seconds

Folder: 17 - RECOMMENDER: 100k
Folder: 17 - MAE: 0.6757
Folder: 17 - TRAIN TIME: 1.264 seconds
Folder: 17 - EVALUATION TIME: 3.026 seconds

Folder: 18 - RECOMMENDER: 100k
Folder: 18 - MAE: 0.6883
Folder: 18 - TRAIN TIME: 1.263 seconds
Folder: 18 - EVALUATION TIME: 3.042 seconds

Folder: 19 - RECOMMENDER: 100k
Folder: 19 - MAE: 0.6849
Folder: 19 - TRAIN TIME: 1.279 seconds
Folder: 19 - EVALUATION TIME: 3.042 seconds

Folder: 20 - RECOMMENDER: 100k
Folder: 20 - MAE: 0.6826
Folder: 20 - TRAIN TIME: 1.279 seconds
Folder: 20 - EVALUATION TIME: 3.042 seconds

Folder: 21 - RECOMMENDER: 100k
Folder: 21 - MAE: 0.6839
Folder: 21 - TRAIN TIME: 1.358 seconds
Folder: 21 - EVALUATION TIME: 3.104 seconds

Folder: 22 - RECOMMENDER: 100k
Folder: 22 - MAE: 0.6847
Folder: 22 - TRAIN TIME: 1.248 seconds
Folder: 22 - EVALUATION TIME: 3.026 seconds

Folder: 23 - RECOMMENDER: 100k
Folder: 23 - MAE: 0.6889
Folder: 23 - TRAIN TIME: 1.45 seconds
Folder: 23 - EVALUATION TIME: 3.011 seconds

Folder: 24 - RECOMMENDER: 100k
Folder: 24 - MAE: 0.6738
Folder: 24 - TRAIN TIME: 1.326 seconds
Folder: 24 - EVALUATION TIME: 3.042 seconds

Folder: 25 - RECOMMENDER: 100k
Folder: 25 - MAE: 0.6838
Folder: 25 - TRAIN TIME: 1.264 seconds
Folder: 25 - EVALUATION TIME: 3.042 seconds

Folder: 26 - RECOMMENDER: 100k
Folder: 26 - MAE: 0.6815
Folder: 26 - TRAIN TIME: 1.311 seconds
Folder: 26 - EVALUATION TIME: 3.026 seconds

Folder: 27 - RECOMMENDER: 100k
Folder: 27 - MAE: 0.6848
Folder: 27 - TRAIN TIME: 1.263 seconds
Folder: 27 - EVALUATION TIME: 3.011 seconds

Folder: 28 - RECOMMENDER: 100k
Folder: 28 - MAE: 0.6836
Folder: 28 - TRAIN TIME: 1.264 seconds
Folder: 28 - EVALUATION TIME: 3.026 seconds

Folder: 29 - RECOMMENDER: 100k
Folder: 29 - MAE: 0.6796
Folder: 29 - TRAIN TIME: 1.373 seconds
Folder: 29 - EVALUATION TIME: 3.089 seconds

Folder: 30 - RECOMMENDER: 100k
Folder: 30 - MAE: 0.6863
Folder: 30 - TRAIN TIME: 1.264 seconds
Folder: 30 - EVALUATION TIME: 3.042 seconds

Folder: 31 - RECOMMENDER: 100k
Folder: 31 - MAE: 0.6831
Folder: 31 - TRAIN TIME: 1.248 seconds
Folder: 31 - EVALUATION TIME: 3.042 seconds

Folder: 32 - RECOMMENDER: 100k
Folder: 32 - MAE: 0.6967
Folder: 32 - TRAIN TIME: 1.264 seconds
Folder: 32 - EVALUATION TIME: 3.073 seconds

Folder: 33 - RECOMMENDER: 100k
Folder: 33 - MAE: 0.6915
Folder: 33 - TRAIN TIME: 1.31 seconds
Folder: 33 - EVALUATION TIME: 3.073 seconds

Folder: 34 - RECOMMENDER: 100k
Folder: 34 - MAE: 0.6811
Folder: 34 - TRAIN TIME: 1.264 seconds
Folder: 34 - EVALUATION TIME: 3.042 seconds

Folder: 35 - RECOMMENDER: 100k
Folder: 35 - MAE: 0.684
Folder: 35 - TRAIN TIME: 1.279 seconds
Folder: 35 - EVALUATION TIME: 3.026 seconds

Folder: 36 - RECOMMENDER: 100k
Folder: 36 - MAE: 0.6852
Folder: 36 - TRAIN TIME: 1.279 seconds
Folder: 36 - EVALUATION TIME: 3.073 seconds

Folder: 37 - RECOMMENDER: 100k
Folder: 37 - MAE: 0.6788
Folder: 37 - TRAIN TIME: 1.31 seconds
Folder: 37 - EVALUATION TIME: 3.074 seconds

Folder: 38 - RECOMMENDER: 100k
Folder: 38 - MAE: 0.6741
Folder: 38 - TRAIN TIME: 1.264 seconds
Folder: 38 - EVALUATION TIME: 3.042 seconds

Folder: 39 - RECOMMENDER: 100k
Folder: 39 - MAE: 0.6736
Folder: 39 - TRAIN TIME: 1.264 seconds
Folder: 39 - EVALUATION TIME: 3.026 seconds

Folder: 40 - RECOMMENDER: 100k
Folder: 40 - MAE: 0.6788
Folder: 40 - TRAIN TIME: 1.404 seconds
Folder: 40 - EVALUATION TIME: 3.042 seconds

Folder: 41 - RECOMMENDER: 100k
Folder: 41 - MAE: 0.6827
Folder: 41 - TRAIN TIME: 1.28 seconds
Folder: 41 - EVALUATION TIME: 3.057 seconds

Folder: 42 - RECOMMENDER: 100k
Folder: 42 - MAE: 0.6754
Folder: 42 - TRAIN TIME: 1.279 seconds
Folder: 42 - EVALUATION TIME: 3.042 seconds

Folder: 43 - RECOMMENDER: 100k
Folder: 43 - MAE: 0.6789
Folder: 43 - TRAIN TIME: 1.264 seconds
Folder: 43 - EVALUATION TIME: 3.057 seconds

Folder: 44 - RECOMMENDER: 100k
Folder: 44 - MAE: 0.6965
Folder: 44 - TRAIN TIME: 1.295 seconds
Folder: 44 - EVALUATION TIME: 3.105 seconds

Folder: 45 - RECOMMENDER: 100k
Folder: 45 - MAE: 0.6825
Folder: 45 - TRAIN TIME: 1.342 seconds
Folder: 45 - EVALUATION TIME: 3.042 seconds

Folder: 46 - RECOMMENDER: 100k
Folder: 46 - MAE: 0.6883
Folder: 46 - TRAIN TIME: 1.279 seconds
Folder: 46 - EVALUATION TIME: 3.042 seconds

Folder: 47 - RECOMMENDER: 100k
Folder: 47 - MAE: 0.6801
Folder: 47 - TRAIN TIME: 1.263 seconds
Folder: 47 - EVALUATION TIME: 3.074 seconds

Folder: 48 - RECOMMENDER: 100k
Folder: 48 - MAE: 0.6761
Folder: 48 - TRAIN TIME: 1.263 seconds
Folder: 48 - EVALUATION TIME: 3.074 seconds

Folder: 49 - RECOMMENDER: 100k
Folder: 49 - MAE: 0.6834
Folder: 49 - TRAIN TIME: 1.264 seconds
Folder: 49 - EVALUATION TIME: 3.057 seconds

Folder: 50 - RECOMMENDER: 100k
Folder: 50 - MAE: 0.6857
Folder: 50 - TRAIN TIME: 1.294 seconds
Folder: 50 - EVALUATION TIME: 3.042 seconds

======================================================
Folder: 0 - RECOMMENDER: CLASSIC
Folder: 0 - MAE: 0.68267
Folder: 0 - TRAIN TIME: 1.30042 seconds
Folder: 0 - EVALUATION TIME: 3.0525999999999995 seconds

