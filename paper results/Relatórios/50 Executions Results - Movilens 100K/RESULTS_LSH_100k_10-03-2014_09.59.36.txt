DATABASE NAME: 100k
FOLDERS: 5
DATABASES PER FOLDER: 10 of trainning and 10 of testing.
K: 30

Folder: 1 - RECOMMENDER: 100k
Folder: 1 - MAE: 0.7638
Folder: 1 - TRAIN TIME: 0.114 seconds
Folder: 1 - EVALUATION TIME: 8.012 seconds

Folder: 2 - RECOMMENDER: 100k
Folder: 2 - MAE: 0.7509
Folder: 2 - TRAIN TIME: 0.094 seconds
Folder: 2 - EVALUATION TIME: 4.185 seconds

Folder: 3 - RECOMMENDER: 100k
Folder: 3 - MAE: 0.7579
Folder: 3 - TRAIN TIME: 0.164 seconds
Folder: 3 - EVALUATION TIME: 4.385 seconds

Folder: 4 - RECOMMENDER: 100k
Folder: 4 - MAE: 0.7477
Folder: 4 - TRAIN TIME: 0.142 seconds
Folder: 4 - EVALUATION TIME: 7.598 seconds

Folder: 5 - RECOMMENDER: 100k
Folder: 5 - MAE: 0.7597
Folder: 5 - TRAIN TIME: 0.155 seconds
Folder: 5 - EVALUATION TIME: 6.254 seconds

Folder: 6 - RECOMMENDER: 100k
Folder: 6 - MAE: 0.7575
Folder: 6 - TRAIN TIME: 0.166 seconds
Folder: 6 - EVALUATION TIME: 5.162 seconds

Folder: 7 - RECOMMENDER: 100k
Folder: 7 - MAE: 0.7651
Folder: 7 - TRAIN TIME: 0.061 seconds
Folder: 7 - EVALUATION TIME: 6.154 seconds

Folder: 8 - RECOMMENDER: 100k
Folder: 8 - MAE: 0.7533
Folder: 8 - TRAIN TIME: 0.162 seconds
Folder: 8 - EVALUATION TIME: 5.964 seconds

Folder: 9 - RECOMMENDER: 100k
Folder: 9 - MAE: 0.763
Folder: 9 - TRAIN TIME: 0.032 seconds
Folder: 9 - EVALUATION TIME: 4.974 seconds

Folder: 10 - RECOMMENDER: 100k
Folder: 10 - MAE: 0.7531
Folder: 10 - TRAIN TIME: 0.163 seconds
Folder: 10 - EVALUATION TIME: 5.075 seconds

Folder: 11 - RECOMMENDER: 100k
Folder: 11 - MAE: 0.7599
Folder: 11 - TRAIN TIME: 0.162 seconds
Folder: 11 - EVALUATION TIME: 7.633 seconds

Folder: 12 - RECOMMENDER: 100k
Folder: 12 - MAE: 0.7635
Folder: 12 - TRAIN TIME: 0.163 seconds
Folder: 12 - EVALUATION TIME: 8.445 seconds

Folder: 13 - RECOMMENDER: 100k
Folder: 13 - MAE: 0.7555
Folder: 13 - TRAIN TIME: 0.161 seconds
Folder: 13 - EVALUATION TIME: 4.081 seconds

Folder: 14 - RECOMMENDER: 100k
Folder: 14 - MAE: 0.7621
Folder: 14 - TRAIN TIME: 0.033 seconds
Folder: 14 - EVALUATION TIME: 5.553 seconds

Folder: 15 - RECOMMENDER: 100k
Folder: 15 - MAE: 0.7555
Folder: 15 - TRAIN TIME: 0.163 seconds
Folder: 15 - EVALUATION TIME: 10.076 seconds

Folder: 16 - RECOMMENDER: 100k
Folder: 16 - MAE: 0.7468
Folder: 16 - TRAIN TIME: 0.032 seconds
Folder: 16 - EVALUATION TIME: 5.401 seconds

Folder: 17 - RECOMMENDER: 100k
Folder: 17 - MAE: 0.7557
Folder: 17 - TRAIN TIME: 0.167 seconds
Folder: 17 - EVALUATION TIME: 8.447 seconds

Folder: 18 - RECOMMENDER: 100k
Folder: 18 - MAE: 0.7671
Folder: 18 - TRAIN TIME: 0.164 seconds
Folder: 18 - EVALUATION TIME: 4.479 seconds

Folder: 19 - RECOMMENDER: 100k
Folder: 19 - MAE: 0.7655
Folder: 19 - TRAIN TIME: 0.163 seconds
Folder: 19 - EVALUATION TIME: 8.097 seconds

Folder: 20 - RECOMMENDER: 100k
Folder: 20 - MAE: 0.7644
Folder: 20 - TRAIN TIME: 0.165 seconds
Folder: 20 - EVALUATION TIME: 7.013 seconds

Folder: 21 - RECOMMENDER: 100k
Folder: 21 - MAE: 0.757
Folder: 21 - TRAIN TIME: 0.162 seconds
Folder: 21 - EVALUATION TIME: 11.056 seconds

Folder: 22 - RECOMMENDER: 100k
Folder: 22 - MAE: 0.7591
Folder: 22 - TRAIN TIME: 0.031 seconds
Folder: 22 - EVALUATION TIME: 11.628 seconds

Folder: 23 - RECOMMENDER: 100k
Folder: 23 - MAE: 0.7739
Folder: 23 - TRAIN TIME: 0.163 seconds
Folder: 23 - EVALUATION TIME: 8.184 seconds

Folder: 24 - RECOMMENDER: 100k
Folder: 24 - MAE: 0.7401
Folder: 24 - TRAIN TIME: 0.163 seconds
Folder: 24 - EVALUATION TIME: 6.337 seconds

Folder: 25 - RECOMMENDER: 100k
Folder: 25 - MAE: 0.7558
Folder: 25 - TRAIN TIME: 0.033 seconds
Folder: 25 - EVALUATION TIME: 8.78 seconds

Folder: 26 - RECOMMENDER: 100k
Folder: 26 - MAE: 0.7611
Folder: 26 - TRAIN TIME: 0.163 seconds
Folder: 26 - EVALUATION TIME: 6.156 seconds

Folder: 27 - RECOMMENDER: 100k
Folder: 27 - MAE: 0.7696
Folder: 27 - TRAIN TIME: 0.134 seconds
Folder: 27 - EVALUATION TIME: 4.458 seconds

Folder: 28 - RECOMMENDER: 100k
Folder: 28 - MAE: 0.7584
Folder: 28 - TRAIN TIME: 0.164 seconds
Folder: 28 - EVALUATION TIME: 6.165 seconds

Folder: 29 - RECOMMENDER: 100k
Folder: 29 - MAE: 0.7672
Folder: 29 - TRAIN TIME: 0.166 seconds
Folder: 29 - EVALUATION TIME: 4.14 seconds

Folder: 30 - RECOMMENDER: 100k
Folder: 30 - MAE: 0.7623
Folder: 30 - TRAIN TIME: 0.163 seconds
Folder: 30 - EVALUATION TIME: 5.897 seconds

Folder: 31 - RECOMMENDER: 100k
Folder: 31 - MAE: 0.7565
Folder: 31 - TRAIN TIME: 0.164 seconds
Folder: 31 - EVALUATION TIME: 5.416 seconds

Folder: 32 - RECOMMENDER: 100k
Folder: 32 - MAE: 0.7591
Folder: 32 - TRAIN TIME: 0.047 seconds
Folder: 32 - EVALUATION TIME: 3.391 seconds

Folder: 33 - RECOMMENDER: 100k
Folder: 33 - MAE: 0.7677
Folder: 33 - TRAIN TIME: 0.165 seconds
Folder: 33 - EVALUATION TIME: 9.335 seconds

Folder: 34 - RECOMMENDER: 100k
Folder: 34 - MAE: 0.7645
Folder: 34 - TRAIN TIME: 0.165 seconds
Folder: 34 - EVALUATION TIME: 9.965 seconds

Folder: 35 - RECOMMENDER: 100k
Folder: 35 - MAE: 0.7572
Folder: 35 - TRAIN TIME: 0.174 seconds
Folder: 35 - EVALUATION TIME: 6.996 seconds

Folder: 36 - RECOMMENDER: 100k
Folder: 36 - MAE: 0.7575
Folder: 36 - TRAIN TIME: 0.162 seconds
Folder: 36 - EVALUATION TIME: 7.495 seconds

Folder: 37 - RECOMMENDER: 100k
Folder: 37 - MAE: 0.7555
Folder: 37 - TRAIN TIME: 0.095 seconds
Folder: 37 - EVALUATION TIME: 8.498 seconds

Folder: 38 - RECOMMENDER: 100k
Folder: 38 - MAE: 0.757
Folder: 38 - TRAIN TIME: 0.055 seconds
Folder: 38 - EVALUATION TIME: 5.416 seconds

Folder: 39 - RECOMMENDER: 100k
Folder: 39 - MAE: 0.76
Folder: 39 - TRAIN TIME: 0.031 seconds
Folder: 39 - EVALUATION TIME: 7.794 seconds

Folder: 40 - RECOMMENDER: 100k
Folder: 40 - MAE: 0.7454
Folder: 40 - TRAIN TIME: 0.032 seconds
Folder: 40 - EVALUATION TIME: 7.441 seconds

Folder: 41 - RECOMMENDER: 100k
Folder: 41 - MAE: 0.7608
Folder: 41 - TRAIN TIME: 0.031 seconds
Folder: 41 - EVALUATION TIME: 4.388 seconds

Folder: 42 - RECOMMENDER: 100k
Folder: 42 - MAE: 0.7637
Folder: 42 - TRAIN TIME: 0.032 seconds
Folder: 42 - EVALUATION TIME: 11.575 seconds

Folder: 43 - RECOMMENDER: 100k
Folder: 43 - MAE: 0.7577
Folder: 43 - TRAIN TIME: 0.164 seconds
Folder: 43 - EVALUATION TIME: 7.829 seconds

Folder: 44 - RECOMMENDER: 100k
Folder: 44 - MAE: 0.7674
Folder: 44 - TRAIN TIME: 0.032 seconds
Folder: 44 - EVALUATION TIME: 4.942 seconds

Folder: 45 - RECOMMENDER: 100k
Folder: 45 - MAE: 0.7577
Folder: 45 - TRAIN TIME: 0.032 seconds
Folder: 45 - EVALUATION TIME: 6.902 seconds

Folder: 46 - RECOMMENDER: 100k
Folder: 46 - MAE: 0.7651
Folder: 46 - TRAIN TIME: 0.032 seconds
Folder: 46 - EVALUATION TIME: 8.653 seconds

Folder: 47 - RECOMMENDER: 100k
Folder: 47 - MAE: 0.7512
Folder: 47 - TRAIN TIME: 0.163 seconds
Folder: 47 - EVALUATION TIME: 10.142 seconds

Folder: 48 - RECOMMENDER: 100k
Folder: 48 - MAE: 0.7584
Folder: 48 - TRAIN TIME: 0.139 seconds
Folder: 48 - EVALUATION TIME: 7.168 seconds

Folder: 49 - RECOMMENDER: 100k
Folder: 49 - MAE: 0.7546
Folder: 49 - TRAIN TIME: 0.031 seconds
Folder: 49 - EVALUATION TIME: 6.685 seconds

Folder: 50 - RECOMMENDER: 100k
Folder: 50 - MAE: 0.7516
Folder: 50 - TRAIN TIME: 0.032 seconds
Folder: 50 - EVALUATION TIME: 7.428 seconds

======================================================
Folder: 0 - RECOMMENDER: LSH
Folder: 0 - MAE: 0.7588219999999999
Folder: 0 - TRAIN TIME: 0.11492 seconds
Folder: 0 - EVALUATION TIME: 6.944959999999999 seconds

