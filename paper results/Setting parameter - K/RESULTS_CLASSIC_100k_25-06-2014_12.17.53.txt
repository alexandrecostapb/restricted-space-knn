DATABASE NAME: 100k
FOLDERS: 1
DATABASES PER FOLDER: 10 of trainning and 10 of testing.
K: 80

Folder: 1 - RECOMMENDER: 100k
Folder: 1 - MAE: 0.693
Folder: 1 - TRAIN TIME: 3.221 seconds
Folder: 1 - EVALUATION TIME: 2.283 seconds

Folder: 2 - RECOMMENDER: 100k
Folder: 2 - MAE: 0.6802
Folder: 2 - TRAIN TIME: 2.904 seconds
Folder: 2 - EVALUATION TIME: 3.054 seconds

Folder: 3 - RECOMMENDER: 100k
Folder: 3 - MAE: 0.6833
Folder: 3 - TRAIN TIME: 2.806 seconds
Folder: 3 - EVALUATION TIME: 3.186 seconds

Folder: 4 - RECOMMENDER: 100k
Folder: 4 - MAE: 0.6796
Folder: 4 - TRAIN TIME: 3.06 seconds
Folder: 4 - EVALUATION TIME: 3.129 seconds

Folder: 5 - RECOMMENDER: 100k
Folder: 5 - MAE: 0.6892
Folder: 5 - TRAIN TIME: 2.102 seconds
Folder: 5 - EVALUATION TIME: 2.805 seconds

Folder: 6 - RECOMMENDER: 100k
Folder: 6 - MAE: 0.6834
Folder: 6 - TRAIN TIME: 1.819 seconds
Folder: 6 - EVALUATION TIME: 2.986 seconds

Folder: 7 - RECOMMENDER: 100k
Folder: 7 - MAE: 0.6862
Folder: 7 - TRAIN TIME: 2.559 seconds
Folder: 7 - EVALUATION TIME: 2.608 seconds

Folder: 8 - RECOMMENDER: 100k
Folder: 8 - MAE: 0.6758
Folder: 8 - TRAIN TIME: 2.941 seconds
Folder: 8 - EVALUATION TIME: 2.883 seconds

Folder: 9 - RECOMMENDER: 100k
Folder: 9 - MAE: 0.688
Folder: 9 - TRAIN TIME: 2.587 seconds
Folder: 9 - EVALUATION TIME: 2.928 seconds

Folder: 10 - RECOMMENDER: 100k
Folder: 10 - MAE: 0.6905
Folder: 10 - TRAIN TIME: 2.555 seconds
Folder: 10 - EVALUATION TIME: 3.328 seconds

======================================================
Folder: 0 - RECOMMENDER: CLASSIC
Folder: 0 - MAE: 0.68492
Folder: 0 - TRAIN TIME: 2.6554 seconds
Folder: 0 - EVALUATION TIME: 2.9189999999999996 seconds

