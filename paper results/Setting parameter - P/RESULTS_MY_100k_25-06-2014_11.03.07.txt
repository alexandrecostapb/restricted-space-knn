DATABASE NAME: 100k
FOLDERS: 1
DATABASES PER FOLDER: 10 of trainning and 10 of testing.
K: 30

Folder: 1 - RECOMMENDER: 100k
Folder: 1 - MAE: 0.6904
Folder: 1 - TRAIN TIME: 1.79 seconds
Folder: 1 - EVALUATION TIME: 1.927 seconds

Folder: 2 - RECOMMENDER: 100k
Folder: 2 - MAE: 0.6818
Folder: 2 - TRAIN TIME: 2.0 seconds
Folder: 2 - EVALUATION TIME: 1.318 seconds

Folder: 3 - RECOMMENDER: 100k
Folder: 3 - MAE: 0.6815
Folder: 3 - TRAIN TIME: 2.07 seconds
Folder: 3 - EVALUATION TIME: 1.153 seconds

Folder: 4 - RECOMMENDER: 100k
Folder: 4 - MAE: 0.6849
Folder: 4 - TRAIN TIME: 1.845 seconds
Folder: 4 - EVALUATION TIME: 1.655 seconds

Folder: 5 - RECOMMENDER: 100k
Folder: 5 - MAE: 0.6898
Folder: 5 - TRAIN TIME: 2.037 seconds
Folder: 5 - EVALUATION TIME: 1.556 seconds

Folder: 6 - RECOMMENDER: 100k
Folder: 6 - MAE: 0.6821
Folder: 6 - TRAIN TIME: 1.746 seconds
Folder: 6 - EVALUATION TIME: 1.412 seconds

Folder: 7 - RECOMMENDER: 100k
Folder: 7 - MAE: 0.6864
Folder: 7 - TRAIN TIME: 1.475 seconds
Folder: 7 - EVALUATION TIME: 0.983 seconds

Folder: 8 - RECOMMENDER: 100k
Folder: 8 - MAE: 0.6737
Folder: 8 - TRAIN TIME: 2.004 seconds
Folder: 8 - EVALUATION TIME: 1.576 seconds

Folder: 9 - RECOMMENDER: 100k
Folder: 9 - MAE: 0.686
Folder: 9 - TRAIN TIME: 1.79 seconds
Folder: 9 - EVALUATION TIME: 1.63 seconds

Folder: 10 - RECOMMENDER: 100k
Folder: 10 - MAE: 0.6896
Folder: 10 - TRAIN TIME: 1.406 seconds
Folder: 10 - EVALUATION TIME: 0.823 seconds

======================================================
Folder: 0 - RECOMMENDER: MY
Folder: 0 - MAE: 0.68462
Folder: 0 - TRAIN TIME: 1.8162999999999996 seconds
Folder: 0 - EVALUATION TIME: 1.4033000000000002 seconds

