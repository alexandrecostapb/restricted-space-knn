DATABASE NAME: 100k
FOLDERS: 1
DATABASES PER FOLDER: 10 of trainning and 10 of testing.
K: 30

Folder: 1 - RECOMMENDER: 100k
Folder: 1 - MAE: 0.69
Folder: 1 - TRAIN TIME: 1.278 seconds
Folder: 1 - EVALUATION TIME: 1.573 seconds

Folder: 2 - RECOMMENDER: 100k
Folder: 2 - MAE: 0.6848
Folder: 2 - TRAIN TIME: 2.075 seconds
Folder: 2 - EVALUATION TIME: 1.474 seconds

Folder: 3 - RECOMMENDER: 100k
Folder: 3 - MAE: 0.6823
Folder: 3 - TRAIN TIME: 1.432 seconds
Folder: 3 - EVALUATION TIME: 1.564 seconds

Folder: 4 - RECOMMENDER: 100k
Folder: 4 - MAE: 0.6855
Folder: 4 - TRAIN TIME: 1.848 seconds
Folder: 4 - EVALUATION TIME: 1.47 seconds

Folder: 5 - RECOMMENDER: 100k
Folder: 5 - MAE: 0.6922
Folder: 5 - TRAIN TIME: 1.716 seconds
Folder: 5 - EVALUATION TIME: 1.375 seconds

Folder: 6 - RECOMMENDER: 100k
Folder: 6 - MAE: 0.6818
Folder: 6 - TRAIN TIME: 2.018 seconds
Folder: 6 - EVALUATION TIME: 1.303 seconds

Folder: 7 - RECOMMENDER: 100k
Folder: 7 - MAE: 0.6867
Folder: 7 - TRAIN TIME: 1.418 seconds
Folder: 7 - EVALUATION TIME: 1.464 seconds

Folder: 8 - RECOMMENDER: 100k
Folder: 8 - MAE: 0.6768
Folder: 8 - TRAIN TIME: 1.676 seconds
Folder: 8 - EVALUATION TIME: 1.317 seconds

Folder: 9 - RECOMMENDER: 100k
Folder: 9 - MAE: 0.6866
Folder: 9 - TRAIN TIME: 1.472 seconds
Folder: 9 - EVALUATION TIME: 0.959 seconds

Folder: 10 - RECOMMENDER: 100k
Folder: 10 - MAE: 0.6906
Folder: 10 - TRAIN TIME: 1.43 seconds
Folder: 10 - EVALUATION TIME: 1.397 seconds

======================================================
Folder: 0 - RECOMMENDER: MY
Folder: 0 - MAE: 0.6857300000000001
Folder: 0 - TRAIN TIME: 1.6362999999999999 seconds
Folder: 0 - EVALUATION TIME: 1.3896000000000002 seconds

